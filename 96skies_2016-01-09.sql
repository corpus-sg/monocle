# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.7.10)
# Database: 96skies
# Generation Time: 2016-01-09 09:14:53 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table countries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `countries`;

CREATE TABLE `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `nationality` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;

INSERT INTO `countries` (`id`, `name`, `nationality`)
VALUES
	(1,'Afghanistan','Afghan'),
	(2,'Albania','Albanian'),
	(3,'Algeria','Algerian'),
	(4,'Andorra','Andorran'),
	(5,'Angola','Angolan'),
	(6,'Antigua and Barbuda','Antiguans, Barbudans'),
	(7,'Argentina','Argentinean'),
	(8,'Armenia','Armenian'),
	(9,'Australia','Australian'),
	(10,'Austria','Austrian'),
	(11,'Azerbaijan','Azerbaijani'),
	(12,'The Bahamas','Bahamian'),
	(13,'Bahrain','Bahraini'),
	(14,'Bangladesh','Bangladeshi'),
	(15,'Barbados','Barbadian'),
	(16,'Belarus','Belarusian'),
	(17,'Belgium','Belgian'),
	(18,'Belize','Belizean'),
	(19,'Benin','Beninese'),
	(20,'Bhutan','Bhutanese'),
	(21,'Bolivia','Bolivian'),
	(22,'Bosnia and Herzegovina','Bosnian, Herzegovinian'),
	(23,'Botswana','Motswana (singular), Batswana (plural)'),
	(24,'Brazil','Brazilian'),
	(25,'Brunei','Bruneian'),
	(26,'Bulgaria','Bulgarian'),
	(27,'Burkina Faso','Burkinabe'),
	(28,'Burundi','Burundian'),
	(29,'Cambodia','Cambodian'),
	(30,'Cameroon','Cameroonian'),
	(31,'Canada','Canadian'),
	(32,'Cape Verde','Cape Verdian'),
	(33,'Central African Republic','Central African'),
	(34,'Chad','Chadian'),
	(35,'Chile','Chilean'),
	(36,'China','Chinese'),
	(37,'Colombia','Colombian'),
	(38,'Comoros','Comoran'),
	(39,'Congo, Republic of the','Congolese'),
	(40,'Congo, Democratic Republic of the','Congolese'),
	(41,'Costa Rica','Costa Rican'),
	(42,'Cote d\'Ivoire','Ivorian'),
	(43,'Croatia','Croatian'),
	(44,'Cuba','Cuban'),
	(45,'Cyprus','Cypriot'),
	(46,'Czech Republic','Czech'),
	(47,'Denmark','Danish'),
	(48,'Djibouti','Djibouti'),
	(49,'Dominica','Dominican'),
	(50,'Dominican Republic','Dominican'),
	(51,'East Timor','East Timorese'),
	(52,'Ecuador','Ecuadorean'),
	(53,'Egypt','Egyptian'),
	(54,'El Salvador','Salvadoran'),
	(55,'Equatorial Guinea','Equatorial Guinean'),
	(56,'Eritrea','Eritrean'),
	(57,'Estonia','Estonian'),
	(58,'Ethiopia','Ethiopian'),
	(59,'Fiji','Fijian'),
	(60,'Finland','Finnish'),
	(61,'France','French'),
	(62,'Gabon','Gabonese'),
	(63,'The Gambia','Gambian'),
	(64,'Georgia','Georgian'),
	(65,'Germany','German'),
	(66,'Ghana','Ghanaian'),
	(67,'Greece','Greek'),
	(68,'Grenada','Grenadian'),
	(69,'Guatemala','Guatemalan'),
	(70,'Guinea','Guinean'),
	(71,'Guinea-Bissau','Guinea-Bissauan'),
	(72,'Guyana','Guyanese'),
	(73,'Haiti','Haitian'),
	(74,'Honduras','Honduran'),
	(75,'Hungary','Hungarian'),
	(76,'Iceland','Icelander'),
	(77,'India','Indian'),
	(78,'Indonesia','Indonesian'),
	(79,'Iran','Iranian'),
	(80,'Iraq','Iraqi'),
	(81,'Ireland','Irish'),
	(82,'Israel','Israeli'),
	(83,'Italy','Italian'),
	(84,'Jamaica','Jamaican'),
	(85,'Japan','Japanese'),
	(86,'Jordan','Jordanian'),
	(87,'Kazakhstan','Kazakhstani'),
	(88,'Kenya','Kenyan'),
	(89,'Kiribati','I-Kiribati'),
	(90,'Korea, North','North Korean'),
	(91,'Korea, South','South Korean'),
	(92,'Kuwait','Kuwaiti'),
	(93,'Kyrgyz Republic','Kirghiz'),
	(94,'Laos','Laotian'),
	(95,'Latvia','Latvian'),
	(96,'Lebanon','Lebanese'),
	(97,'Lesotho','Mosotho'),
	(98,'Liberia','Liberian'),
	(99,'Libya','Libyan'),
	(100,'Liechtenstein','Liechtensteiner'),
	(101,'Lithuania','Lithuanian'),
	(102,'Luxembourg','Luxembourger'),
	(103,'Macedonia','Macedonian'),
	(104,'Madagascar','Malagasy'),
	(105,'Malawi','Malawian'),
	(106,'Malaysia','Malaysian'),
	(107,'Maldives','Maldivan'),
	(108,'Mali','Malian'),
	(109,'Malta','Maltese'),
	(110,'Marshall Islands','Marshallese'),
	(111,'Mauritania','Mauritanian'),
	(112,'Mauritius','Mauritian'),
	(113,'Mexico','Mexican'),
	(114,'Federated States of Micronesia','Micronesian'),
	(115,'Moldova','Moldovan'),
	(116,'Monaco','Monegasque'),
	(117,'Mongolia','Mongolian'),
	(118,'Morocco','Moroccan'),
	(119,'Mozambique','Mozambican'),
	(120,'Myanmar (Burma)','Burmese'),
	(121,'Namibia','Namibian'),
	(122,'Nauru','Nauruan'),
	(123,'Nepal','Nepalese'),
	(124,'Netherlands','Dutch'),
	(125,'New Zealand','New Zealander'),
	(126,'Nicaragua','Nicaraguan'),
	(127,'Niger','Nigerien'),
	(128,'Nigeria','Nigerian'),
	(129,'Norway','Norwegian'),
	(130,'Oman','Omani'),
	(131,'Pakistan','Pakistani'),
	(132,'Palau','Palauan'),
	(133,'Panama','Panamanian'),
	(134,'Papua New Guinea','Papua New Guinean'),
	(135,'Paraguay','Paraguayan'),
	(136,'Peru','Peruvian'),
	(137,'Philippines','Filipino'),
	(138,'Poland','Polish'),
	(139,'Portugal','Portuguese'),
	(140,'Qatar','Qatari'),
	(141,'Romania','Romanian'),
	(142,'Russia','Russian'),
	(143,'Rwanda','Rwandan'),
	(144,'Saint Kitts and Nevis','Kittian and Nevisian'),
	(145,'Saint Lucia','Saint Lucian'),
	(146,'Samoa','Samoan'),
	(147,'San Marino','Sammarinese'),
	(148,'Sao Tome and Principe','Sao Tomean'),
	(149,'Saudi Arabia','Saudi Arabian'),
	(150,'Senegal','Senegalese'),
	(151,'Serbia and Montenegro','Serbian'),
	(152,'Seychelles','Seychellois'),
	(153,'Sierra Leone','Sierra Leonean'),
	(154,'Singapore','Singaporean'),
	(155,'Slovakia','Slovak'),
	(156,'Slovenia','Slovene'),
	(157,'Solomon Islands','Solomon Islander'),
	(158,'Somalia','Somali'),
	(159,'South Africa','South African'),
	(160,'Spain','Spanish'),
	(161,'Sri Lanka','Sri Lankan'),
	(162,'Sudan','Sudanese'),
	(163,'Suriname','Surinamer'),
	(164,'Swaziland','Swazi'),
	(165,'Sweden','Swedish'),
	(166,'Switzerland','Swiss'),
	(167,'Syria','Syrian'),
	(168,'Taiwan','Taiwanese'),
	(169,'Tajikistan','Tadzhik'),
	(170,'Tanzania','Tanzanian'),
	(171,'Thailand','Thai'),
	(172,'Togo','Togolese'),
	(173,'Tonga','Tongan'),
	(174,'Trinidad and Tobago','Trinidadian'),
	(175,'Tunisia','Tunisian'),
	(176,'Turkey','Turkish'),
	(177,'Turkmenistan','Turkmen'),
	(178,'Tuvalu','Tuvaluan'),
	(179,'Uganda','Ugandan'),
	(180,'Ukraine','Ukrainian'),
	(181,'United Arab Emirates','Emirian'),
	(182,'United Kingdom','British'),
	(183,'United States','American'),
	(184,'Uruguay','Uruguayan'),
	(185,'Uzbekistan','Uzbekistani'),
	(186,'Vanuatu','Ni-Vanuatu'),
	(187,'Vatican City (Holy See)','none'),
	(188,'Venezuela','Venezuelan'),
	(189,'Vietnam','Vietnamese'),
	(190,'Yemen','Yemeni'),
	(191,'Zambia','Zambian'),
	(192,'Zimbabwe','Zimbabwean');

/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table files
# ------------------------------------------------------------

DROP TABLE IF EXISTS `files`;

CREATE TABLE `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(128) NOT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `started_at` datetime NOT NULL,
  `finished_at` datetime NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;

INSERT INTO `files` (`id`, `file_name`, `type`, `status`, `started_at`, `finished_at`, `user_id`, `updated_at`, `created_at`)
VALUES
	(231,'GWT - Upload File_20150224083226.csv',1,1,'2015-02-24 08:32:26','2015-02-24 08:32:32',27,'2015-02-24 08:32:27','2015-02-24 08:32:27'),
	(236,'GWT - Upload File_20150224083721.csv',1,1,'2015-02-24 08:37:21','2015-02-24 08:37:26',27,'2015-02-24 08:37:22','2015-02-24 08:37:22'),
	(237,'GWT - Upload File_20150224083909.csv',1,1,'2015-02-24 08:39:09','2015-02-24 08:39:14',27,'2015-02-24 08:39:10','2015-02-24 08:39:10'),
	(238,'Normal - Upload File_20150224083939.xlsx',1,1,'2015-02-24 08:39:39','2015-02-24 08:39:50',27,'2015-02-24 08:39:40','2015-02-24 08:39:40'),
	(242,'GWT - Upload File_20150224084448.csv',1,1,'2015-02-24 08:44:48','2015-02-24 08:45:24',27,'2015-02-24 08:44:49','2015-02-24 08:44:49'),
	(245,'Normal - Upload File_20150224094402.xlsx',1,1,'2015-02-24 09:44:04','2015-02-24 09:44:11',27,'2015-02-24 09:44:05','2015-02-24 09:44:05'),
	(246,'GWT - Upload File_20150224094402.csv',1,1,'2015-02-24 09:44:04','2015-02-24 09:44:07',27,'2015-02-24 09:44:05','2015-02-24 09:44:05'),
	(249,'Normal - Upload File_20150224203130.xlsx',1,1,'2015-02-24 20:31:32','2015-02-24 20:31:38',27,'2015-02-24 20:31:33','2015-02-24 20:31:33'),
	(251,'GWT - Upload File_20150225132943.csv',1,1,'2015-02-25 13:29:45','2015-02-25 13:29:48',27,'2015-02-25 13:29:46','2015-02-25 13:29:46'),
	(303,'GWT - Upload File_20150304093917.csv',1,1,'2015-03-04 09:39:18','2015-03-04 09:39:20',27,'2015-03-04 09:39:19','2015-03-04 09:39:19'),
	(309,'SimilarWeb - Upload File_20150305003549.xlsx',3,1,'2015-03-05 00:36:43','2015-03-05 01:29:03',27,'2015-03-05 00:36:44','2015-03-05 00:36:44'),
	(310,'Normal - Upload File_20150305003707.xlsx',1,1,'2015-03-05 00:37:11','2015-03-05 00:37:17',27,'2015-03-05 00:37:12','2015-03-05 00:37:12'),
	(311,'GWT - Upload File_20150305003707.csv',1,1,'2015-03-05 00:37:11','2015-03-05 00:37:13',27,'2015-03-05 00:37:12','2015-03-05 00:37:12'),
	(312,'GWT - Export File_20150305003707.csv',1,1,'2015-03-05 00:37:11','2015-03-05 00:37:12',27,'2015-03-05 00:37:12','2015-03-05 00:37:12'),
	(314,'Normal - Export File_20150305010259.csv',1,1,'2015-03-05 01:03:01','2015-03-05 01:03:06',27,'2015-03-05 01:03:02','2015-03-05 01:03:02'),
	(315,'GWT - Upload File_20150305010259.csv',1,1,'2015-03-05 01:03:01','2015-03-05 01:03:04',27,'2015-03-05 01:03:02','2015-03-05 01:03:02'),
	(316,'Normal - Upload File_20150313114519.xlsx',1,1,'2015-03-13 11:45:21','2015-03-13 11:45:26',27,'2015-03-13 11:45:22','2015-03-13 11:45:22'),
	(317,'SW Etsy Organic 1840 KB 2nd try_20150318235404_20150322102638.xlsx',3,1,'2015-03-22 10:47:11','2015-03-22 11:02:04',27,'2015-03-22 10:47:12','2015-03-22 10:47:12'),
	(318,'SW Etsy Organic 1840 KB 2nd try_20150318235404_20150322131744.xlsx',3,1,'2015-03-22 13:18:43','2015-03-22 13:34:12',27,'2015-03-22 13:18:44','2015-03-22 13:18:44'),
	(319,'SimilarWeb - Upload File_20150322144215.xlsx',3,1,'2015-03-22 14:43:06','2015-03-22 15:24:44',27,'2015-03-22 14:43:07','2015-03-22 14:43:07'),
	(320,'SimilarWeb - Upload File_20150322145030.xlsx',3,1,'2015-03-22 14:51:06','2015-03-22 15:32:29',27,'2015-03-22 14:51:07','2015-03-22 14:51:07'),
	(321,'Normal - Upload File_20150322162130.MB',1,1,'2015-03-22 16:21:32','2015-03-22 16:21:37',27,'2015-03-22 16:21:33','2015-03-22 16:21:33'),
	(322,'Normal - Upload File_20150322162231.MB',1,1,'2015-03-22 16:22:34','2015-03-22 16:22:39',27,'2015-03-22 16:22:35','2015-03-22 16:22:35'),
	(323,'Normal - Upload File_20150322162319.MB',1,1,'2015-03-22 16:23:21','2015-03-22 16:23:25',27,'2015-03-22 16:23:22','2015-03-22 16:23:22'),
	(324,'Normal - Upload File_20150322162425.MB',1,1,'2015-03-22 16:24:26','2015-03-22 16:24:31',27,'2015-03-22 16:24:27','2015-03-22 16:24:27'),
	(325,'SimilarWeb - Upload File_20150322162522.xlsx',3,1,'2015-03-22 16:25:41','2015-03-22 17:06:49',27,'2015-03-22 16:25:42','2015-03-22 16:25:42'),
	(326,'GWT - Upload File_20150322162557.MB',2,1,'2015-03-22 16:25:59','2015-03-22 16:26:02',27,'2015-03-22 16:26:00','2015-03-22 16:26:00'),
	(330,'Normal - Upload File_20150520201636.xlsx',1,1,'2015-05-20 20:16:38','2015-05-20 20:16:41',27,'2015-05-20 20:16:38','2015-05-20 20:16:38'),
	(331,'SimilarWeb - Upload File_20150520202111.xlsx',3,1,'2015-05-20 20:21:31','2015-05-20 21:09:15',27,'2015-05-20 20:21:31','2015-05-20 20:21:31'),
	(332,'Normal - Upload File_20150520210346.xlsx',1,1,'2015-05-20 21:03:48','2015-05-20 21:03:55',27,'2015-05-20 21:03:48','2015-05-20 21:03:48'),
	(333,'GWT - Upload File_20150522152935.csv',2,1,'2015-05-22 15:29:40','2015-05-22 15:29:41',27,'2015-05-22 15:29:40','2015-05-22 15:29:40'),
	(334,'Normal - Upload File_20150522153003.xlsx',1,1,'2015-05-22 15:30:11','2015-05-22 15:30:15',27,'2015-05-22 15:30:11','2015-05-22 15:30:11'),
	(335,'GWT - Upload File_20150522184241.csv',2,1,'2015-05-22 18:42:44','2015-05-22 18:42:46',27,'2015-05-22 18:42:44','2015-05-22 18:42:44'),
	(336,'Normal - Upload File_20150529010356.MB',1,1,'2015-05-29 01:03:57','2015-05-29 01:04:03',28,'2015-05-29 01:03:57','2015-05-29 01:03:57'),
	(337,'Normal - Upload File_20150529010342.xlsx',1,1,'2015-05-29 01:03:57','2015-05-29 01:04:03',28,'2015-05-29 01:03:57','2015-05-29 01:03:57'),
	(344,'Normal - Upload File_20150604114224.xlsx',1,1,'2015-06-04 11:42:25','2015-06-04 11:42:27',27,'2015-06-04 11:42:25','2015-06-04 11:42:25'),
	(345,'Normal - Upload File_20150604114335.xlsx',1,1,'2015-06-04 11:43:36','2015-06-04 11:43:38',27,'2015-06-04 11:43:36','2015-06-04 11:43:36'),
	(346,'Normal - Upload File_20150604114720.xlsx',1,1,'2015-06-04 11:47:21','2015-06-04 11:47:24',27,'2015-06-04 11:47:21','2015-06-04 11:47:21'),
	(350,'Normal - Upload File_20150605130315.xlsx',1,1,'2015-06-05 13:03:16','2015-06-05 13:03:19',27,'2015-06-05 13:03:16','2015-06-05 13:03:16'),
	(356,'Normal - Upload File_20150606080418.xlsx',1,1,'2015-06-06 08:04:20','2015-06-06 08:04:23',27,'2015-06-06 08:04:20','2015-06-06 08:04:20'),
	(358,'Normal - Upload File_20150606113157.xlsx',1,1,'2015-06-06 11:31:58','2015-06-06 11:32:01',27,'2015-06-06 11:31:58','2015-06-06 11:31:58'),
	(360,'Normal - Upload File_20150606120019.xlsx',1,1,'2015-06-06 12:00:20','2015-06-06 12:00:23',27,'2015-06-06 12:00:20','2015-06-06 12:00:20'),
	(364,'Normal - Upload File_20150608105741.xlsx',1,1,'2015-06-08 10:57:42','2015-06-08 10:57:45',27,'2015-06-08 10:57:42','2015-06-08 10:57:42'),
	(380,'Search-(amazon_20150615160025.com)-(2015_04_01-2015_05_31)',3,1,'2015-06-15 16:00:34','2015-06-15 16:10:39',35,'2015-06-15 16:00:34','2015-06-15 16:00:34'),
	(381,'xls-file-large_20150615160158.xls',1,1,'2015-06-15 16:02:06','2015-06-15 16:08:58',36,'2015-06-15 16:02:06','2015-06-15 16:02:06'),
	(382,'xlsx-file_20150617222355.xlsx',1,1,'2015-06-17 22:24:19','2015-06-17 22:24:25',35,'2015-06-17 22:24:19','2015-06-17 22:24:19'),
	(384,'Search-(alibaba_20150617222331.com)-(2015_04_01-2015_05_31)',3,1,'2015-06-17 22:24:20','2015-06-17 22:40:29',36,'2015-06-17 22:24:20','2015-06-17 22:24:20'),
	(385,'Normal - Upload File_20150621015356.xlsx',1,1,'2015-06-21 01:53:57','2015-06-21 01:54:00',27,'2015-06-21 01:53:57','2015-06-21 01:53:57'),
	(392,'Search-(epicentreasia_20150627214612.com)-(2014_06_01-2015_05_31)',3,1,'2015-06-27 21:47:07','2015-06-27 21:47:11',27,'2015-06-27 21:47:07','2015-06-27 21:47:07'),
	(393,'Search-(zappos_20150627214636.com)-(2015_05_01-2015_05_31)',3,1,'2015-06-27 21:47:07','2015-06-27 21:50:10',27,'2015-06-27 21:47:07','2015-06-27 21:47:07'),
	(394,'Search-(amazon_20150627214635.com)-(2015_04_01-2015_05_31)',3,1,'2015-06-27 21:47:07','2015-06-27 21:54:58',27,'2015-06-27 21:47:07','2015-06-27 21:47:07'),
	(395,'Search-(alibaba_20150627214635.com)-(2015_04_01-2015_05_31)',3,1,'2015-06-27 21:47:07','0000-00-00 00:00:00',27,'2015-06-27 21:47:07','2015-06-27 21:47:07'),
	(396,'xls-file-large_20150628234301.xls',1,1,'2015-06-28 23:43:07','2015-06-28 23:46:16',35,'2015-06-28 23:43:07','2015-06-28 23:43:07'),
	(397,'Search-(alibaba_20150628235339.com)-(2015_03_01-2015_05_31)',3,1,'2015-06-28 23:54:28','2015-06-29 00:18:01',35,'2015-06-28 23:54:28','2015-06-28 23:54:28'),
	(398,'Search-(epicentreasia_20150629002103.com)-(2014_06_01-2015_05_31)',3,1,'2015-06-29 00:23:46','2015-06-29 00:23:49',36,'2015-06-29 00:23:46','2015-06-29 00:23:46'),
	(399,'Search-(zappos_20150629002103.com)-(2015_05_01-2015_05_31)',3,1,'2015-06-29 00:23:46','2015-06-29 00:24:54',36,'2015-06-29 00:23:46','2015-06-29 00:23:46'),
	(400,'Search-(zappos_20150629002112.com)-(2015_05_01-2015_05_31)',3,1,'2015-06-29 00:25:04','2015-06-29 00:27:51',35,'2015-06-29 00:25:04','2015-06-29 00:25:04'),
	(401,'Search-(epicentreasia_20150629002109.com)-(2014_06_01-2015_05_31)',3,1,'2015-06-29 00:25:04','2015-06-29 00:25:08',35,'2015-06-29 00:25:04','2015-06-29 00:25:04'),
	(403,'Search-(alibaba_20150629002119.com)-(2015_04_01-2015_05_31)',3,1,'2015-06-29 00:25:25','2015-06-29 00:39:23',35,'2015-06-29 00:25:25','2015-06-29 00:25:25'),
	(404,'xls-file-large_20150705232151.xls',1,1,'2015-07-05 23:23:23','2015-07-05 23:26:33',37,'2015-07-05 23:23:23','2015-07-05 23:23:23'),
	(405,'www-tinkeredge-com_20141202T055648Z_TopSearchQueries_20141101-20141201_20150705234058.csv',2,1,'2015-07-05 23:41:07','2015-07-05 23:41:10',37,'2015-07-05 23:41:07','2015-07-05 23:41:07'),
	(408,'Search-(zappos_20150705235810.com)-(2015_05_01-2015_05_31)',3,1,'2015-07-05 23:58:55','2015-07-06 00:00:03',37,'2015-07-05 23:58:55','2015-07-05 23:58:55'),
	(409,'Search-(alibaba_20151003183830.com)-(2015_04_01-2015_05_31)',3,1,'2015-10-03 18:38:53','0000-00-00 00:00:00',35,'2015-10-03 18:38:53','2015-10-03 18:38:53'),
	(410,'Search-(amazon_20151003184011.com)-(2015_04_01-2015_05_31)',3,1,'2015-10-03 18:40:26','0000-00-00 00:00:00',35,'2015-10-03 18:40:26','2015-10-03 18:40:26'),
	(411,'Search-(zappos_20151003185146.com)-(2015_05_01-2015_05_31)',3,1,'2015-10-03 18:53:10','2015-10-03 18:56:14',35,'2015-10-03 18:53:10','2015-10-03 18:53:10'),
	(412,'Search-(epicentreasia_20151003184904.com)-(2014_06_01-2015_05_31)',3,1,'2015-10-03 18:53:10','2015-10-03 18:53:13',35,'2015-10-03 18:53:10','2015-10-03 18:53:10'),
	(413,'Search-(amazon_20151003185224.com)-(2015_04_01-2015_05_31)',3,1,'2015-10-03 18:53:10','2015-10-03 19:01:11',35,'2015-10-03 18:53:10','2015-10-03 18:53:10'),
	(414,'Search-(alibaba_20151003185254.com)-(2015_04_01-2015_05_31)',3,1,'2015-10-03 18:53:11','0000-00-00 00:00:00',35,'2015-10-03 18:53:11','2015-10-03 18:53:11');

/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table job_functions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `job_functions`;

CREATE TABLE `job_functions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `job_functions` WRITE;
/*!40000 ALTER TABLE `job_functions` DISABLE KEYS */;

INSERT INTO `job_functions` (`id`, `name`)
VALUES
	(1,'Account Manager'),
	(2,'Account Planner'),
	(3,'Analytics'),
	(4,'CEO'),
	(5,'CMO'),
	(6,'Creative'),
	(7,'Freelance'),
	(8,'Head of Ecommerce'),
	(9,'Head of Online'),
	(10,'Media Buyer'),
	(11,'Media Planner'),
	(12,'PPC Specialist'),
	(13,'Sales Rep'),
	(14,'SEO');

/*!40000 ALTER TABLE `job_functions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_roles`;

CREATE TABLE `user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;

INSERT INTO `user_roles` (`id`, `name`)
VALUES
	(1,'Admin'),
	(2,'User');

/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(128) NOT NULL,
  `last_name` varchar(128) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `company` varchar(80) NOT NULL,
  `job_function` varchar(32) NOT NULL,
  `updates` int(11) NOT NULL DEFAULT '1',
  `country` varchar(75) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `role` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `remember_token` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `company`, `job_function`, `updates`, `country`, `created_at`, `updated_at`, `updated_by`, `role`, `status`, `remember_token`)
VALUES
	(27,'Ricky ','Halim','halim.rick@gmail.com','$2y$10$EU1JjMs718P1Nc/oALcXa.3FrQkt6E/k4.gNsRlaDyHv3vQIIP0RO','Ratat','Freelance',1,'Indonesia','2015-03-04 02:09:34','2015-06-20 17:57:41',27,1,1,'to899OYNDXDgW7inzQVfLgsXJ3YqpBCt5CdLHuqiXy5uye1IErHjN30zY0jo'),
	(28,'test','tester','test@gmail.com','$2y$10$oVhkKFnWXZ7YQFnbgwECrOE45xyrY81b2il2EbxsYtJ/TNUP1nuDa','testing','Account Manager',1,'Singapore','2015-05-26 16:45:23','2015-06-03 16:49:33',0,1,1,'1jdJEBAJkCtySPBFyN6P0LMUpC3MG5naXhGrQjw5EviWc98gv5h1UASP8sRs'),
	(33,'TAd','ta','yah@gmail.com','$2y$10$hWkmg.Y0BtKn46OMWHEd9OQaTgxY9IFZBHH31UDPPEbC8EvTLkj/i','421','Account Manager',1,'Singapore','2015-05-30 17:51:02','2015-06-03 16:49:53',0,2,1,''),
	(35,'Cheok','Lup','cheoklup@tinkeredge.com','$2y$10$m/H5cyOS6HUB38GxR25Aq.4Rw7DEUuIJN7pWBFctRv/QKuC.RGtmO','Monocle','CEO',0,'Singapore','2015-06-04 00:34:15','2015-10-10 14:52:46',35,1,1,'1uVputZTwkGt8UJ615BVN0Jy5BD2IRh2oS0MyUo2wWz999PCuVQH0yFoWRkw'),
	(36,'Cheok Lup','Wong','ahlupz@gmail.com','$2y$10$ejyv1IycFquuq7lK19tQJeS.eOYI1aXpLptfJR6jfw3.A.WjnJot2','96 Skies','SEO',0,'Singapore','2015-06-06 20:37:10','2015-10-10 14:51:54',0,2,1,'nLuqrWaknBDVoaJXiBkdeJmkKIW43cPbf4AtYf7cH86U8wWWk1TUlcI9yiOT'),
	(37,'Koh','Mei Yi','genin.koh@gmail.com','$2y$10$/39nPjUsTX.0RXbXfHwhbu5LSeaNWj4Hfyr/SFgr4lHLNlCF1WLg6','Global Sources','Account Manager',0,'Singapore','2015-06-17 22:19:33','2015-06-17 14:51:07',0,2,1,'qUghgijL3n69lESxJPRdTfs62mYQ3JRTPN8B5tCJBXL6AlLCK7QCU2qwe6BP');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
