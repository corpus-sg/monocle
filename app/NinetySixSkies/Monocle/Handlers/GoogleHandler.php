<?php

namespace NinetySixSkies\Monocle\Handlers;

use NinetySixSkies\Monocle\Util\HeaderFormatter;
use Config;
use Excel;
use File;
use FileEntry;
use DB;
use GoogleFile;
use GoogleResult;
/**
* 
*/
class GoogleHandler implements JobHandlerInterface
{
	const QUERIES = 'queries';  
	const CLICKS = 'clicks';
	const IMPRESSIONS = 'impressions'; 
	const CTR = 'ctr';
	const POSITION = 'position';

	protected $payload;
	protected $file;
	protected $fileName;
	protected $progress=0;
	
	function __construct($payload)
	{
		$this->payload = $payload;
		$this->file = FileEntry::where('file_name', '=', $this->payload['files'])->first();
		$this->fileName = Config::get('uploads.google').$this->payload['files'];
	}

	public function handle() 
	{
		echo "Processing started...\n";
		
		$this->saveRawData();
		$this->saveTokens();
		$this->processCalculation();
		$this->updateProgressLog(1, 1, 100, 100);
		$this->file->finished_at = date('Y-m-d H:i:s');
		$this->file->save();
	}

	public function saveRawData()
	{
		echo "Saving raw ... ";
		$uploads = array();
		DB::beginTransaction();
		try {
			// dd($this->fileName);
			Excel::selectSheetsByIndex(0)->load($this->fileName, function($reader) {
				$rows = $reader->all();
				$size = count($rows);
				$uploads = array();
				
				foreach ($rows as $i=>$row) {
					$uploads[$i][self::QUERIES] = $row->{self::QUERIES};
					$uploads[$i][self::CLICKS] = $row->{self::CLICKS};
					$uploads[$i][self::IMPRESSIONS] = $row->{self::IMPRESSIONS};
					$uploads[$i][self::CTR] = $row->{self::CTR};
					$uploads[$i][self::POSITION] = $row->{self::POSITION};
					$uploads[$i]['file_id'] = $this->file->id;
					
					if (($i+1)%1000==0) {
						DB::table('google_raw_files')->insert($uploads);
						DB::commit();
						$this->updateProgressLog($i, $size, 0, 50);
						$uploads = array();
					}
				}

				DB::table('google_raw_files')->insert($uploads);
				DB::commit();
				$this->updateProgressLog(1, 1, 0, 50);

			}, 'UTF-8');
			
		} catch (Exception $e) {
			DB::rollback();
			return false;
		}

		return true;
	}

	public function saveTokens()
	{
		echo "Saving unique words... ";
		$results = array();
		$queries = GoogleFile::where('file_id', '=', $this->file->id)->lists('queries');

		DB::beginTransaction();
		try {
			foreach ($queries as $i => $query) {
				$token = strtok($query, $this->payload['delimeter']);
				while($token !== false) {
					$uniqueWord = trim($token);
					$token = strtok($this->payload['delimeter']);

					if (isset($results[$uniqueWord])) {
						$results[$uniqueWord]['frequency'] += 1;
					} else {
						$results[$uniqueWord]['unique_word'] = $uniqueWord;
						$results[$uniqueWord]['frequency'] = 1;
						$results[$uniqueWord]['character_count'] = strlen($uniqueWord);
						$results[$uniqueWord]['file_id'] = $this->file->id;
					}
				}
			}

			DB::table('google_files')->insert($results);
			DB::commit();
			$this->updateProgressLog(1, 1, 50, 75);
		} catch (Exception $e) {
			DB::rollback();
			return false;
		}

		echo "Complete\n";
	}

	public function processCalculation()
	{
		echo "Calculating figures... ";
		$results = GoogleResult::where('file_id', '=', $this->file->id)->get();
		$size = count($results);
		DB::beginTransaction();
		try {
			foreach ($results as $i=>$result) {
				$stats = DB::select("
					SELECT SUM(clicks) as summed_clicks, SUM(impressions) as summed_impression, CAST(SUM(clicks)/SUM(impressions) AS DECIMAL(20,15)) AS summed_ctr, CAST(AVG(position) AS DECIMAL(20,15)) AS summed_avg_position FROM google_raw_files WHERE file_id=? AND queries LIKE ?", 
					array($this->file->id, "%$result->unique_word%"));

				$result->summed_clicks = $stats[0]->summed_clicks;
				$result->summed_impression = $stats[0]->summed_impression;
				$result->summed_ctr = $stats[0]->summed_ctr;
				$result->summed_avg_position = $stats[0]->summed_avg_position;
				$result->save();

				if (($i+1)%1000==0 || count($i-1==$size)) {
					$this->updateProgressLog($i, $size, 75, 100);
				}
				DB::commit();
			}
		} catch (Exception $e) {
			DB::rollback();
			return false;
		}
		echo "Complete\n";
	}

	public function updateProgressLog($current, $total, $min, $max)
	{
		$progress = round($current/$total * ($max - $min) + $min);
		if ($progress>100) {
			$progress = 100;
		}
		if ($progress > $this->progress) {
			$this->progress = $progress;
			File::put(Config::get('uploads.tmp').$this->file->getTmpName().".tmp", $this->file->getTmpName().";".$progress);
			echo $progress."\n";
		}
	}
}