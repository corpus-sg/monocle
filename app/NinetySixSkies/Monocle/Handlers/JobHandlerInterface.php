<?php

namespace NinetySixSkies\Monocle\Handlers;

interface JobHandlerInterface
{
	public function handle();
}