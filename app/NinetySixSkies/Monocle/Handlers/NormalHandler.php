<?php

namespace NinetySixSkies\Monocle\Handlers;

use Config;
use Excel;
use FileEntry;
use File;
use DB;
/**
*
*/
class NormalHandler implements JobHandlerInterface
{
	protected $payload;

	function __construct($payload)
	{
		$this->payload = $payload;
	}

	public function handle()
	{
		$error = false;
		DB::beginTransaction();

		try {
			// $file =
			$fileEntry = FileEntry::where('file_name', '=', $this->payload['files'])->first();
			$file = Config::get('uploads.normal').$this->payload['files'];
			// $header = $this->getHeader($file, 0);
			$uploads = array();
			$progress = 0;
			Excel::selectSheetsByIndex(0)->load($file, function ($reader) use ($fileEntry, &$uploads, &$progress) {
				$rows = $reader->noHeading()->toArray();
				$size = count($rows);
				File::put(Config::get('uploads.tmp').$fileEntry->getTmpName().".tmp", $fileEntry->getTmpName().";0");
				foreach ($rows as $i=>$row) {
					// dd($row);
					$token = strtok($row[0], $this->payload['delimeter']);
					while($token !== false) {
						$uniqueWord = trim($token);
						$token = strtok($this->payload['delimeter']);
						if (isset($uploads[$uniqueWord])) {
							$uploads[$uniqueWord]['frequency'] = $uploads[$uniqueWord]['frequency'] + 1;
						} else {
							$uploads[$uniqueWord]['unique_word'] = $uniqueWord;
							$uploads[$uniqueWord]['frequency'] = 1;
							$uploads[$uniqueWord]['character_count'] = strlen($uniqueWord);
							$uploads[$uniqueWord]['file_id'] = $fileEntry->id;
						}

						$current = round($i/$size*100/2);
						if ($current>$progress) {
							$progress=$current;
							echo "Progress: $progress% Completed.\n";
							File::put(Config::get('uploads.tmp').$fileEntry->getTmpName().".tmp", $fileEntry->getTmpName().";".$progress);
						}
					}
				}
			});

			$size = count($uploads);
			$i = 0;
			foreach ($uploads as $key => $upload) {
				$i++;
				DB::table('normal_files')->insert($upload);
				if ($i>=$size) {
					DB::commit();
					File::put(Config::get('uploads.tmp').$fileEntry->getTmpName().".tmp", $fileEntry->getTmpName().";99");
				} elseif ($i==1000) {
					DB::commit();
					$i=0;
					$progress += round(1000/$size*100/2);
					echo "Progress: $progress% Completed.\n";
					File::put(Config::get('uploads.tmp').$fileEntry->getTmpName().".tmp", $fileEntry->getTmpName().";".$progress);
				}
			}
			$fileEntry->finished_at = date('Y-m-d H:i:s');
			$fileEntry->save();
			DB::commit();
			File::put(Config::get('uploads.tmp').$fileEntry->getTmpName().".tmp", $fileEntry->getTmpName().";100");
		} catch (Exception $e) {
			DB::rollback();
			$error = true;
			return $e->getMessage();
		}

		return 'Completed';
	}

	public function getHeaders($file)
	{
		return Excel::selectSheetsByIndex(0)
			->load($file)
            ->noHeading()
            ->ignoreEmpty()
            ->limit(1)
            ->first()
            ->toArray();
	}

	public function getHeader($file, $index)
	{
		return $this->getHeaders($file)[$index];
	}
}
