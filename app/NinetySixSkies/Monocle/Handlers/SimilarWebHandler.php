<?php

namespace NinetySixSkies\Monocle\Handlers;

use NinetySixSkies\Monocle\Util\HeaderFormatter;
use Config;
use Excel;
use File;
use FileEntry;
use DB;
use SimilarWebFile;
use SimilarWebResult;
/**
*
*/
class SimilarWebHandler implements JobHandlerInterface
{
	const SEARCH_TERMS = 'search_terms';
	const TRAFFIC_SHARE = 'traffic_share';

	protected $payload;
	protected $file;
	protected $fileName;
	protected $progress=0;


	function __construct($payload)
	{
		$this->payload = $payload;
		$this->file = FileEntry::where('file_name', '=', $this->payload['files'])->first();
		$this->fileName = Config::get('uploads.similarweb').$this->payload['files'];
	}

	public function handle()
	{
		$this->saveRawData();
		$this->saveTokens();
		$this->processCalculation();
		$this->updateProgressLog(1, 1, 100, 100);
		$this->file->finished_at = date('Y-m-d H:i:s');
		$this->file->save();
	}

	public function saveRawData()
	{
		echo "Saving raw ... ";
		$uploads = array();
		DB::beginTransaction();
		try {
			Excel::selectSheetsByIndex(0)->load($this->fileName, function($reader) {
				$rows = $reader->all();
				$size = count($rows);
				$uploads = array();

				foreach ($rows as $i=>$row) {
					$traffic_share = $row->{self::TRAFFIC_SHARE};
					if (strpos($traffic_share, "%") !== false) {
						$traffic_share = str_replace("%", "", $traffic_share)/100;
					}

					$uploads[$i][self::SEARCH_TERMS] = $row->{self::SEARCH_TERMS};
					$uploads[$i][self::TRAFFIC_SHARE] = $traffic_share;
					$uploads[$i]['file_id'] = $this->file->id;

					if (($i+1)%1000==0) {
						DB::table('similarweb_raw_files')->insert($uploads);
						DB::commit();
						$this->updateProgressLog($i, $size, 0, 50);
						$uploads = array();
					}
				}

				DB::table('similarweb_raw_files')->insert($uploads);
				DB::commit();
				$this->updateProgressLog(1, 1, 0, 50);

			}, 'UTF-8');

		} catch (Exception $e) {
			DB::rollback();
			return false;
		}

		return true;
	}

	public function saveTokens()
	{
		echo "Saving unique words... ";
		$results = array();
		$queries = SimilarWebFile::where('file_id', '=', $this->file->id)->lists('search_terms');

		DB::beginTransaction();
		try {
			foreach ($queries as $i => $query) {
				$token = strtok($query, $this->payload['delimeter']);
				while($token !== false) {
					$uniqueWord = trim($token);
					$token = strtok($this->payload['delimeter']);

					if (isset($results[$uniqueWord])) {
						$results[$uniqueWord]['frequency'] += 1;
					} else {
						$results[$uniqueWord]['unique_word'] = $uniqueWord;
						$results[$uniqueWord]['frequency'] = 1;
						$results[$uniqueWord]['character_count'] = strlen($uniqueWord);
						$results[$uniqueWord]['file_id'] = $this->file->id;
					}
				}
			}

			DB::table('similarweb_files')->insert($results);
			DB::commit();
			$this->updateProgressLog(1, 1, 50, 75);
		} catch (Exception $e) {
			DB::rollback();
			return false;
		}

		echo "Complete\n";
		return true;
	}

	public function processCalculation()
	{
		echo "Calculating figures... ";
		$results = SimilarWebResult::where('file_id', '=', $this->file->id)->get();
		$size = count($results);
		DB::beginTransaction();
		try {
			foreach ($results as $i=>$result) {
				$stats = DB::select("
					SELECT CAST(SUM(traffic_share) AS DECIMAL(20,15)) AS summed_traffic_share FROM similarweb_raw_files WHERE file_id=? AND search_terms LIKE ?",
					array($this->file->id, "%$result->unique_word%")
				);

				$result->summed_traffic_share = $stats[0]->summed_traffic_share;
				$result->save();

				if (($i+1)%1000==0 || count($i-1==$size)) {
					$this->updateProgressLog($i, $size, 75, 100);
				}
				DB::commit();
			}
		} catch (Exception $e) {
			DB::rollback();
			return false;
		}
		echo "Complete\n";
		return true;

	}

	public function getHeaders($file)
	{
		return HeaderFormatter::format(
			Excel::selectSheetsByIndex(0)
				->load($file)
	            ->noHeading()
	            ->ignoreEmpty()
	            ->limit(1)
	            ->first()
	            ->toArray()
	        );
	}

	public function getHeader($file, $index)
	{
		return $this->getHeaders($file)[$index];
	}

	public function updateProgressLog($current, $total, $min, $max)
	{
		$progress = round($current/$total * ($max - $min) + $min);
		if ($progress>100) {
			$progress = 100;
		}
		if ($progress > $this->progress) {
			$this->progress = $progress;
			File::put(Config::get('uploads.tmp').$this->file->getTmpName().".tmp", $this->file->getTmpName().";".$progress);
			echo $progress."\n";
		}
	}
}
