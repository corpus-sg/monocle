<?php 

namespace NinetySixSkies\Monocle\Util;

class HeaderFormatter
{
	public static function format($headers)
	{
		foreach ($headers as $i => $header) {
			$header = strtolower(preg_replace('!\s+!', '_', $header));
			$header = strtolower(preg_replace('!_+!', '_', $header));
			$headers[$i] = strtolower(preg_replace('/[^a-zA-Z0-9_]/', '', $header)); 
		}

		return $headers;
	}
}