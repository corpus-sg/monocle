<?php

class CoverController extends BaseController {
/**
* Display a listing of the resource.
*
* @return Response
*/
	protected $layout = "layouts.main";	
	public function index()
	{
        $files = File::allFiles('files');
        // chmod(base_path().'/files', 777);
        $in_progress = FileEntry::curProcessingSheet();

		$files = FileEntry::all();
		$this->layout->content = View::make('main.dashboard', compact('files', 'in_progress'))->with('files',$files);
		
	}

	public function show_files()
	{
		$processingfiles = FileEntry::where('finished_at','=','0000-00-00 00:00:00')->get();
		$finishedfiles = FileEntry::where('finished_at','!=','0000-00-00 00:00:00')->get();
		$this->layout->content = View::make('main.files')->with('processingfiles',$processingfiles)->with('finishedfiles',$finishedfiles);
	}

    public function processed_files()
    {
        
        // return View::make('main.processed_files')->with('files', File::allFiles('files'));
    }

    public function ajaxInProgress() {

        $in_progress = FileEntry::curProcessingSheet();
        $progress = View::make('forms.processing')->with('in_progress', $in_progress);
        return $progress;
    }
	
}
?>