@extends('layouts.main')

@section('header')

 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@stop
@section('content')
<div class="col-md-12">
    <!-- -- BASIC PROGRESS BARS ---->
    <h3><i class="fa fa-angle-right"></i> Dashboard</h3>
    <div class="showback">
        <h4><i class="fa fa-angle-right"></i> Upload your files</h4>
        <div class="col-lg-4 col-md-4 col-sm-4 mb" id="normalsheet">
            <div class="green-panel pn">
                <i class="fa fa-file-o fa-4x"></i>
               
                <p class="title">Normal Sheet</p>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 mb" id="googleweb">
            <div class="blue-panel pn">
                <i class="fa fa-google fa-4x"></i>
               
                <p class="title">Google Webmaster</p>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 mb" id="similiarweb">
            <div class="red-panel pn">
                <i class="fa fa-globe fa-4x"></i>
               
                <p class="title">Similiar Web</p>
            </div>
        </div>
        <div class="clear"></div>
    </div><!--/showback -->
  	<div class="showback">
    	<h4><i class="fa fa-angle-right"></i> Running / Completed Jobs</h4>
       
         <table class="table table-bordered table-striped bootstrap-datatable smallerfont datatable">
                  <thead>
                      <tr>
                          <th>File Name</th>
                          <th>Job Option Type</th>
                          <th class="onlydesktop">Job Start</th>
                          <th class="onlydesktop">Job End</th>
                          <th class="onlydesktop">Completion</th>
                          <th>Actions</th>
                      </tr>
                  </thead>   
                  <tbody>
                  <?php $filesname = ""; ?>
                  
                  @foreach ($files as $key=>$value)
                  	<tr>
                    	<td>{{ $value->file_name }}</td>
                    	<td>
                        	@if ($value->type == 1)
                            	Normal 
                            @elseif ($value->type == 2)
                            	Google Webmaster
                            @else
                            	Similiar Web
                            @endif
                        </td>
						<?php
                            $tmpname = explode('.',$value->file_name);
                            $tmpname[0] = str_replace(' ', '', $tmpname[0]);
                            $filesname .= $tmpname[0].".tmp;";
                        ?>
                    	<td class="onlydesktop">{{ date('d-m-Y H:i:s',strtotime($value->started_at)) }}</td>
                    	<td class="onlydesktop" id="{{ $tmpname[0] }}date">
                        	@if ($value->finished_at != "0000-00-00 00:00:00")
	                        	{{ date('d-m-Y H:i:s',strtotime($value->finished_at)) }}
                                <?php $tmpname[0] = ""; ?>
 							@else
                            	---
                            @endif
                        </td>
                    	<td class="onlydesktop" id="{{ $tmpname[0] }}cell">
                        	@if ($value->finished_at != "0000-00-00 00:00:00")
                            	Completed
 							@else
                            	
                                <div class="progress progress-striped active" id="{{ $tmpname[0] }}progress">
                                  <div class="progress-bar" role="progressbar" id="{{ $tmpname[0] }}bar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 1%">
                                    <span class="sr-only">0% Complete</span>
                                  </div>
                                </div>
                            @endif
                        </td>
                    	<td>
							 <?php
                                $name = explode('.',$value->file_name);
                                
                            ?>
                       		@if ($value->finished_at != "0000-00-00 00:00:00")
                            	
                                <a href="files/<?php echo $name[0].'-Report.xlsx'; ?>" download><button type="button" class="btn btn-primary"><i class="fa fa-file-excel-o"></i></button></a>
                                <a href="files/<?php echo $name[0].'-Report.csv'; ?>" download><button type="button" class="btn btn-warning"><i class="fa fa-file-code-o"></i></button></a>
 							@else
                            	<div class="{{ $tmpname[0] }}out">
                        		<button type="button" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</button>
                                </div>
                                <div class="{{ $tmpname[0] }}in none">
                                <a href="files/<?php echo $name[0].'-Report.xlsx'; ?>" download><button type="button" class="btn btn-primary"><i class="fa fa-file-excel-o"></i></button></a>
                                <a href="files/<?php echo $name[0].'-Report.csv'; ?>" download><button type="button" class="btn btn-warning"><i class="fa fa-file-code-o"></i></button></a>
 								</div>
                            @endif
                        </td>
                    </tr>

                 @endforeach
                  </tbody>
         </table>
        <input type="hidden" id="filesname" value="{{ $filesname }}">
    </div>
</div>
{{ Form::open(array('action'=>'ProcessController@upload','id'=>'uploadform','method'=>'put')) }}
	{{ Form::hidden('id',0,array('id'=>'processid')) }}
{{ Form::close() }}

@stop

@section ('script')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script>
runscript();
window.setInterval(function(){

runscript();
}, 2500);
	function runscript(){
		
		var name = $("#filesname").val().split(';');
		for (var n = 0; n < name.length -1; n++)
		{
			var check = false;
			var tmpname = name[n].split('.');
			var request = jQuery.get('http://www.96skies.com/app2/tmp/'+name[n], function(data) {
				//alert(name[n]);
				check = true;
				var tmp = data.split(';');
				$("#"+tmp[0]+"bar").css('width',tmp[1]+"%");
				if (tmp[1] == 100)
				{
					var currentdate = new Date(); 
					var datetime = "" + currentdate.getDate() + "-"
					+  ( '0' + (currentdate.getMonth()+1) ).slice( -2 )  + "-" 
					+ currentdate.getFullYear() + " "  
					+ ( '0' + (currentdate.getHours()) ).slice( -2 ) + ":"  
					+ ( '0' + (currentdate.getMinutes()) ).slice( -2 ) + ":" 
					+ ( '0' + (currentdate.getSeconds()) ).slice( -2 );
					$("#"+tmp[0]+"bar").css("width",100).delay(500).fadeOut('slow','swing',function(){
					
					$("#"+tmp[0]+"cell").html("Completed");
					$("#"+tmp[0]+"date").html(datetime);
					});
					//alert(tmpname[0]);
					$("."+tmp[0]+"out").delay(500).fadeOut('slow','swing',function(){
						
					$("."+tmp[0]+"in").fadeIn();
					});
				}
			});
			request.success(function(result) {
			  console.log(result);
			});
			
			request.error(function(jqXHR, textStatus, errorThrown) {
			  if (textStatus == 'timeout')
				alert('The server is not responding');
			
			  if (textStatus == 'error'){
				  if(errorThrown == "Not Found")
				  {
				  }
			  }
			  // Etc
			});
		}
	}
	$('.datatable').DataTable({ "order": [[ 2, "desc" ]]});
	$("#normalsheet").click(function(){
		$("#processid").val(1);
		$("#uploadform").submit();
	});
	$("#googleweb").click(function(){
		$("#processid").val(2);
		$("#uploadform").submit();
	});
	$("#similiarweb").click(function(){
		$("#processid").val(3);
		$("#uploadform").submit();
	});

</script>
@stop