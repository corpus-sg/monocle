@extends('layouts.main')

    @section('header')
     <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css"> -->
    @stop
    @section('content')
    <div class="col-md-12">
        <!-- BASIC PROGRESS BARS -->
        <h3><i class="fa fa-angle-right"></i> Upload your files</h3>
        <div class="showback">
            <h4><i class="fa fa-angle-right"></i> 
            	@if ($id == 1)
                	Normal Sheet
                @elseif ($id == 2)
                	Google Webmaster
                @else
                	Similiar Web
                @endif
            
            </h4>
            <div class="horizontalLine"></div>
            @include('include.delimiter', ['id' => $id])            
            @include('include.multi_upload', array('id'=>$id))                
            @include('include.upload_script')
            <!-- @include('include.upload_js_script') -->
            {{ Form::open(array('action'=>'ProcessController@start_job','id'=>'startjob')) }}
            <input type="hidden" name="processfiles" id="processfiles">
            <input type="hidden" name="type" value="{{ $id }}">
            <button type="submit" class="btn btn-primary start">
                <i class="glyphicon glyphicon-upload"></i>
                <span>Start Job</span>
            </button>
            <a href="{{ URL::to('/') }}"><button type="button" class="btn btn-warning">Back</button></a>
        	{{ Form::close() }}
        </div><!--/showback -->
    </div><!-- /end col-md-12 -->

    @stop <!-- end content-->
    
    @section('script')
	<script>
		var name= "",idx=0;
		$("#startjob").submit(function(e){
			idx = 0;
			$('.name a').each(function() {
			
				$("#processfiles").val($(this).html());
			  
				$.ajax({
						type: 'post',
						url: 'http://www.96skies.com/app2/start_job',
						cache: false,
						dataType: 'json',
						data: $('#startjob').serialize(),
						beforeSend: function() { 
						},
						success: function(data) {
							if(data.success == false)
							{  
								$('.horizontalLine').html('Something went to wrong.Please Try again later...');	
							} 
							
						},
						error: function(xhr, textStatus, thrownError) {
							$('.horizontalLine').html(thrownError);
							
						}
				});
			});
			window.location.href ='http://www.96skies.com/app2';
           return false;
		   
		});
		</script>
    @stop
 