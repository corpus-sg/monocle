<?php

return array(
	'normal' => '/var/www/monocle/normal/',
	'similarweb' => '/var/www/monocle/similiarweb/',
	'google' => '/var/www/monocle/google/',
	'tmp' => '/var/www/monocle/public/tmp/'
);
