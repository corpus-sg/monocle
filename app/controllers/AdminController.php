<?php

class AdminController extends BaseController {

	protected $layout = "layouts.main";	
	public function show_accounts()
	{
		$users = UserEntry::where('id','!=',Auth::user()->id)->get();
		$this->layout->content =  View::make('admin.listaccounts')->with('users',$users);	
	}
	public function updatesignup()
	{
		$update = UserEntry::where('id','=',Input::get('id'))->pluck('updates');
		if ($update == 0)
			$update = 1;
		else
			$update = 0;
		UserEntry::where('id','=',Input::get('id'))->update(array(
			'updates'=>$update
		));
		return Response::json('1');
	}
	public function delete_user()
	{
		$id = Input::get('id');
		UserEntry::where('id','=',$id)->delete();
		FileEntry::where('user_id','=',$id)->delete();
		return Redirect::to('listaccounts');
	}
	public function changestatus()
	{
		$status = UserEntry::where('id','=',Input::get('id'))->pluck('status');
		if ($status == 0)
			$status = 1;
		else
			$status = 0;
		UserEntry::where('id','=',Input::get('id'))->update(array(
			'status'=>$status
		));
		return Response::json('1');
	}
	public function show_createaccount()
	{
		
		return View::make('admin.createaccount');	
	}
}
?>