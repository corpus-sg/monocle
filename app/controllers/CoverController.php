<?php

class CoverController extends BaseController {

	protected $layout = "layouts.main";	
	public function index()
	{
		// If logged in, show dashboard
		if (Auth::user())
		{
				$files = FileEntry::where('user_id','=',Auth::user()->id)->orderBy('started_at','desc')->get();
				$this->layout->content = View::make('main.dashboard')->with('files',$files);
		}
		// Else Prompt Login Page
		else
		{
			return Redirect::to('login');
		}
	}
	public function show_files()
	{
		$processingfiles = FileEntry::where('user_id','=',Auth::user()->id)->where('finished_at','=','0000-00-00 00:00:00')->orderBy('started_at','desc')->get();
		$finishedfiles = FileEntry::where('user_id','=',Auth::user()->id)->where('finished_at','!=','0000-00-00 00:00:00')->orderBy('started_at','desc')->get();
		$this->layout->content = View::make('main.files')->with('processingfiles',$processingfiles)->with('finishedfiles',$finishedfiles);
	}
	public function logout()
	{
		Auth::logout();
		return Redirect::to('/');
	}
/*
    public function processed_files()
    {
        
        // return View::make('main.processed_files')->with('files', File::allFiles('files'));
    }

    public function ajaxInProgress() {

        $in_progress = FileEntry::curProcessingSheet();
        $progress = View::make('forms.processing')->with('in_progress', $in_progress);
        return $progress;
    }
*/	
}
?>