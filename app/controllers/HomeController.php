<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('layouts.main');
	}

	public function ajaxUserProfile() {
		dd(Input::all());
	}

	public function phpInfo()
	{
		phpinfo();
	}

	public function reset()
	{
		DB::update('TRUNCATE TABLE google_files');
		DB::update('TRUNCATE TABLE similarweb_files');
		DB::update('TRUNCATE TABLE normal_files');
		DB::update('TRUNCATE TABLE files');
		File::deleteDirectory(Config::get('uploads.normal'), true);
		File::deleteDirectory(Config::get('uploads.similarweb'), true);
		File::deleteDirectory(Config::get('uploads.google'), true);
		File::deleteDirectory(Config::get('uploads.google'), true);
		File::deleteDirectory(Config::get('uploads.tmp'), true);
	}

}
