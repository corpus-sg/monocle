<?php
ini_set("memory_limit","7G");
ini_set('max_execution_time', '0');
ini_set('max_input_time', '0');
set_time_limit(0);
ignore_user_abort(true);  

class OldProcessController extends BaseController {
/**
* Display a listing of the resource.
*
* @return Response
*/
	protected $layout = "layouts.main";	
	public function upload()
	{
		$this->setUserId();
		// $user_id = unserialize($_COOKIE['user_profile'])['user_id'] || rand();
		// View::share('user_id', $user_id);

		$id = Input::get('id');
		$this->layout->content = View::make('main.upload')->with('id',$id);
		//return View::make('main.test_multi_upl', compact('id'));
		
	}
	public function process_upload()
	{

		// return Response::json('kkk');
		date_default_timezone_set("Singapore");
		$files = Input::file('files');
		$type = Input::get('type');

		foreach($files as $file) {
			$ext = $file->guessClientExtension(); // (Based on mime type)
			//$ext = $file->getClientOriginalExtension(); // (Based on filename)
			$filename = $file->getClientOriginalName();
			$tmp = explode('.',$filename);
			$finalname = $tmp[0].'_'.date('YmdHis').'.'.$tmp[1];


			$file2 = $file;

			if ($type == 1)
			{
					$file->move('normal/', $finalname);
			                chmod('normal/'.$finalname, 0777); 
			}
			if ($type == 2)
			{
					$file->move('gwt/', $finalname);
			                chmod('gwt/'.$finalname, 0777); 
			}
			if ($type == 3)
			{
					$file->move('similiarweb/', $finalname);
			                chmod('similiarweb/'.$finalname, 0777); 
			}


		
		
		//	 exec('php ./app/controllers/Processing/action.php "'.addslashes($type).'" "'.addslashes($finalname).'" "'.addslashes($id).'"');
			
		}
		if ($type == 1)
			$url = base_path().'/normal/'.$finalname; // Xua la server/files/ ...
		if ($type == 2)
			$url = base_path().'/gwt/'.$finalname; // Xua la server/files/ ...
		if ($type == 3)
			$url = base_path().'/similiarweb/'.$finalname; // Xua la server/files/ ...
		$fileArray = array(
		  
			'name' => $finalname,
			'size' => $file->getClientSize(),
			'url' => $url,
			'deleteUrl' => $url,
			'deleteType' => "DELETE"
		  
		);

		//	exec($this->processExcelCsv($type, $finalname,$id)." > /dev/null 2>/dev/null &");
		$filejson = new stdClass();
		$filejson->files[] = $fileArray;
		return json_encode($filejson);
		
		//View::share('finalname', $finalname);
		//$this->_echoing(serialize($finalname), "test_upload");
		//return Response::json($finalname);
		
		
	}
	public function start_job()
	{
		date_default_timezone_set("Singapore");
		/** Include path **/
		set_include_path(get_include_path() . PATH_SEPARATOR . '../../../Classes/');
		
		/** PHPExcel_IOFactory */
		include 'PHPExcel/IOFactory.php';
		
		//include('config.php');
		
		$type = Input::get('type');
		$files = Input::get('processfiles');
		$FileEntry = new FileEntry;
		$FileEntry->file_name = $files;
		$FileEntry->type= $type;
		$FileEntry->status = 1;
		$FileEntry->started_at = date('Y-m-d H:i:s');
		$FileEntry->finished_at = date('0000-00-00 00:00:00');
		$FileEntry->save();

		$id = DB::getPdo()->lastInsertId();
		$this->processExcelCsv($type, $files);
		
		return Redirect::to('/');
	}
	public function show_files()
	{
		$processingfiles = FileEntry::where('finished_at','=','0000-00-00 00:00:00');
		$finishedfiles = FileEntry::where('finished_at','!=','0000-00-00 00:00:00');
		$this->layout->content = View::make('main.files')->with('processingfiles',$processingfiles)->with('finishedfiles',$finishedfiles);
	}
	public function test()
	{
		while(true){}
	}

	private static function _test() {
		$this->newTmpDir();
		$filetest = public_path(). "/uploads/tmp/test.txt";
		File::put($filetest, " des: test".public_path(). " ". rand());
	}

	private static function _echoing($msg, $filename) {
		$filetest = public_path(). "/uploads/tmp/echo.txt";

		if(!empty($filename)) {
			$filetest = public_path(). "/uploads/tmp/echo_$filename.txt";
		}
		File::put($filetest, " des: test".public_path(). " ". $msg);
	}

	// Create new tempory directory if not exist, this dir store temp image upload
    private function newTmpDir() {
    	$this->newTmpDir();
        $path = public_path(). '/uploads/tmp/';

        if(!File::exists($path)) {
            // path does not exist
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        return;
    }

    private static function processExcelCsv($type, $file) {
		$type = $type;
		$filename = $file;
		include(base_path().'/app/controllers/Processing/action.php');
		
	}
}
?>