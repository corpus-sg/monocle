<?php

ini_set("memory_limit","7G");
ini_set('max_execution_time', '0');
ini_set('max_input_time', '0');
set_time_limit(0);
ignore_user_abort(true);  



class ProcessController extends BaseController {
/**
* Display a listing of the resource.
*
* @return Response
*/
	protected $layout = "layouts.main";	
	public function upload()
	{
		$this->setUserId();
		$id = Input::get('id');
		$this->layout->content = View::make('main.upload')->with('id',$id);
	}
	public function process_upload()
	{
		date_default_timezone_set("Singapore");
		$files = Input::file('files');
		$type = Input::get('type');
		$checker = true;
		foreach($files as $file) {
			
			$ext = $file->guessClientExtension(); 
			$filename = $file->getClientOriginalName();
			$tmp = explode('.',$filename);
			$finalname = $tmp[0].'_'.date('YmdHis').'.'.$tmp[1];
			$file2 = $file;
			
			if ($type == 1)
			{
				$urlfile = Config::get('uploads.normal').$finalname;
					$file->move(Config::get('uploads.normal'), $finalname);
					//chmod($urlfile, 0777); 
			}
			if ($type == 2)
			{
				$urlfile = Config::get('uploads.google').$finalname;
					$file->move(Config::get('uploads.google'), $finalname);
				   // chmod($urlfile, 0777); 
			}
			if ($type == 3)
			{
				$urlfile = Config::get('uploads.similarweb').$finalname;
					$file->move(Config::get('uploads.similarweb'), $finalname);
					//chmod('similiarweb/'.$finalname, 0777); 
			}
			chmod($urlfile, 0777); 
			
			$checker = true;//$this->checker($type,$finalname);
			
			if (!$checker)
			{
				if ($type == 1)
					$errormsg = "Please upload file with data in column A only and without data header.";				
				else if ($type == 2)
					$errormsg = "Please upload the Search Queries Report exported from Google Webmaster Tools.";
				else if ($type == 3)
					$errormsg = "Please upload the Search Terms Report exported from SimilarWeb.";
			}
				
			
			
		}
		if ($type == 1)
			$url = Config::get('uploads.normal').$finalname; // Xua la server/files/ ...
		if ($type == 2)
			$url = Config::get('uploads.google').$finalname; // Xua la server/files/ ...
		if ($type == 3)
			$url = Config::get('uploads.similarweb').$finalname; // Xua la server/files/ ...
		if (!$checker)
		{
			$fileArray = array(
			  
				'name' => $finalname,
				'size' => $file->getClientSize(),
				'deleteUrl' => $url,
				'deleteType' => "DELETE",
				'error' => $errormsg
			  
			);
		}
		else
		{
			$fileArray = array(
			  
				'name' => $finalname,
				'size' => $file->getClientSize(),
				'url' => $url,
				'deleteUrl' => $url,
				'deleteType' => "DELETE"
			  
			);
		}
		$filejson = new stdClass();
		$filejson->files[] = $fileArray;
		return json_encode($filejson);
	}
	public function delete_file()
	{
		$FileEntry = FileEntry::find(Input::get('id'));
		$FileEntry->delete();
		$type = Input::get('type');
		$filename = explode(".",Input::get('filename'));
		if ($type == 1)
		{
			unlink('normal/'.$filename[0].".".$filename[1]);
			unlink('normal/'.$filename[0]."-Report.xlsx");
			unlink('normal/'.$filename[0]."-Report.csv");
		}
		else if ($type == 2)
		{
			unlink('gwt/'.$filename[0].".".$filename[1]);
			unlink('gwt/'.$filename[0]."-Report.xlsx");
			unlink('gwt/'.$filename[0]."-Report.csv");
		}
		else if ($type == 3)
		{
			unlink('similiarweb/'.$filename[0].".".$filename[1]);
			unlink('similiarweb/'.$filename[0]."-Report.xlsx");
			unlink('similiarweb/'.$filename[0]."-Report.csv");
		}
		return Response::json('success');
	}
	public function start_job()
	{
		date_default_timezone_set("Singapore");
		/** Include path **/
		set_include_path(get_include_path() . PATH_SEPARATOR . '../../../Classes/');
		
		/** PHPExcel_IOFactory */
		include 'PHPExcel/IOFactory.php';
		
		//include('config.php');
		$delimeter = "";
		$delimeters = array('\[','`','!','@','#',
		'$','%','^','&','*',
		'(',')','_','+','=',
		'{','}','|','\-','~',
		'\\\\',':','"',';','\'',
		'<','>','?','.',','
		,'\/',' ','\]');
		$idx = 0;
		for ($n = 1; $n < 34; $n++)
		{
			if (isset($_POST['d'.$n]))
				$delimeter .= $delimeters[$n-1];
		}
		$type = Input::get('type');
		$files = Input::get('processfiles');
		$FileEntry = new FileEntry;
		$FileEntry->file_name = $files;
		$FileEntry->type= $type;
		$FileEntry->status = 1;
		$FileEntry->user_id = Auth::user()->id;
		$FileEntry->started_at = date('Y-m-d H:i:s');
		$FileEntry->finished_at = null;
		$FileEntry->save();

		$id = DB::getPdo()->lastInsertId();
		$payload = array('type'=>$type, 'files'=>$files, 'delimeter'=>$delimeter);
		Queue::push('JobHandler', $payload, 'ProcessMonocleJob');
		// $this->processExcelCsv($type, $files,$delimeter);
		
		return 'true';
	}
	public function show_files()
	{
		$processingfiles = FileEntry::where('finished_at','=','0000-00-00 00:00:00');
		$finishedfiles = FileEntry::where('finished_at','!=','0000-00-00 00:00:00');
		$this->layout->content = View::make('main.files')->with('processingfiles',$processingfiles)->with('finishedfiles',$finishedfiles);
	}
	public function test()
	{
		while(true){}
	}

	private static function _test() {
		$this->newTmpDir();
		$filetest = public_path(). "/uploads/tmp/test.txt";
		File::put($filetest, " des: test".public_path(). " ". rand());
	}

	private static function _echoing($msg, $filename) {
		$filetest = public_path(). "/uploads/tmp/echo.txt";

		if(!empty($filename)) {
			$filetest = public_path(). "/uploads/tmp/echo_$filename.txt";
		}
		File::put($filetest, " des: test".public_path(). " ". $msg);
	}

	// Create new tempory directory if not exist, this dir store temp image upload
    private function newTmpDir() {
    	$this->newTmpDir();
        $path = public_path(). '/uploads/tmp/';

        if(!File::exists($path)) {
            // path does not exist
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        return;
    }

    private static function processExcelCsv($type, $file,$delimeter) {
		$type = $type;
		$filename = $file;
		include(base_path().'/app/controllers/Processing/action.php');
	}
	private static function checker($type,$filename) {
		include(base_path().'/app/controllers/Processing/checker.php');
		return $checker;	
	}

	public function download($id, $type)
	{
		$sql = "";
		$fileEntry = FileEntry::find($id);
		$fileName = preg_replace('/\\.[^.\\s]{3,4}$/', '', $fileEntry->file_name);
		$fullPath = "/tmp/".$fileName.".csv";

		if (!File::exists($fullPath)) {
			switch ($fileEntry->type) {
				case 2:
					$sql = "
						SELECT * FROM
						(SELECT 'unique_word', 'frequency', 'character_count', 'summed_clicks', 'summed_impression', 'summed_ctr', 'summed_avg_position'
						UNION ALL (
						SELECT `unique_word`, `frequency`, `character_count`, `summed_clicks`, `summed_impression`, `summed_ctr`, `summed_avg_position`
						FROM google_files
						WHERE `file_id`=?
						ORDER BY `frequency` DESC))";
					break;

				case 3:
					$sql = "
						SELECT * FROM
						(SELECT 'unique_word', 'frequency', 'character_count', 'summed_traffic_share'
						UNION ALL (
						SELECT `unique_word`, `frequency`, `character_count`, `summed_traffic_share`
						FROM similarweb_files
						WHERE `file_id`=?
						ORDER BY `frequency` DESC))";
					break;
				
				default:
					$sql = "
						SELECT * FROM
						(SELECT 'unique_word', 'frequency', 'character_count'
						UNION ALL (
						SELECT `unique_word`, `frequency`, `character_count`
						FROM normal_files
						WHERE `file_id`=?
						ORDER BY `frequency` DESC))";
					break;
			}

			$sql .= " AS export INTO OUTFILE '".$fullPath."' CHARACTER SET utf8 FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\n'";

			$results = DB::select($sql, array($id));
		}

		header("Content-Type: application/csv");
	    header("Content-Disposition: attachment; filename=".$fileName.".csv");
	    header("Content-Length: " . filesize($fullPath));
	    header('Pragma: no-cache');

	    readfile($fullPath);
	    exit();
	}
}