<?php
ini_set("memory_limit","7G");
ini_set('max_execution_time', '0');
ini_set('max_input_time', '0');
set_time_limit(0);
error_reporting(0);
ignore_user_abort(true); 

$extension = pathinfo($filename, PATHINFO_EXTENSION);


if ($type == 1)
	$inputFileName = Config::get('uploads.normal').$filename; // Xua la server/files/ ...

if ($type == 2)
	$inputFileName = Config::get('uploads.google').$filename; // Xua la server/files/ ...
if ($type == 3)
	$inputFileName = Config::get('uploads.similarweb').$filename; // Xua la server/files/ ...

	$check = true;
$filesize = filesize($inputFileName);
while ($check)
{
	$tmpfile = fopen("tmp/counter2.tmp", "r+") or die("Unable to open file!");
	$val = fgets($tmpfile);
	fclose($tmpfile);	
	if ($val + $filesize < 2000000)
	{
		$tmpfile = fopen("tmp/counter.tmp", "w") or die("Unable to open file!");
			
		if (flock($tmpfile, LOCK_EX)) {  // acquire an exclusive lock
			$check = false;
			fwrite($tmpfile, $val+$filesize);
			
			fflush($tmpfile);            // flush output before releasing the lock
			flock($tmpfile, LOCK_UN);    // release the lock
		}
		fclose($tmpfile);	
	}
	sleep(1);
}

if($extension[1] == 'csv')
{
	$objReader = PHPExcel_IOFactory::createReader('csv');
	$objPHPExcel = $objReader->load($inputFileName);
	$sheet = $objPHPExcel->getActiveSheet();
}
else
{
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
	$objReader = PHPExcel_IOFactory::createReader($inputFileType);
	$objPHPExcel = $objReader->load($inputFileName);
	$sheet = $objPHPExcel->getActiveSheet();
}
$check = true;
while ($check)
{
	$tmpfile = fopen("tmp/counter2.tmp", "r+") or die("Unable to open file!");
	$val = fgets($tmpfile);
	fclose($tmpfile);	
	$tmpfile = fopen("tmp/counter.tmp", "w") or die("Unable to open file!");
		
	if (flock($tmpfile, LOCK_EX)) {  // acquire an exclusive lock
		$check = false;
		fwrite($tmpfile, $val-$filesize);
		
		fflush($tmpfile);            // flush output before releasing the lock
		flock($tmpfile, LOCK_UN);    // release the lock
	}
	fclose($tmpfile);	
	
sleep(1);
}
$checker = true;
if ($type == 1)
{	
	
	if ($sheet->getCell("B1") != "" || $sheet->getCell("C1") != "" || $sheet->getCell("D1") != "" || $sheet->getCell("E1") != "" || $sheet->getCell("F1") != "")
		$checker = false;
	if ($sheet->getCell ("A1") == "")
		$checker = false;

}
if ($type == 2)
{
	if ($sheet->getCell("A1") != "Query" || $sheet->getCell("B1") != "Impressions" || $sheet->getCell("D1") != "Clicks" || $sheet->getCell("F1") != "CTR" || $sheet->getCell("H1") != "Avg. position")
		$checker = false;
	if ($sheet->getCell("A2") == "" || $sheet->getCell("B2") == "" || $sheet->getCell("D2") == "" || $sheet->getCell("F2") == "" || $sheet->getCell("H2") == "")
		$checker = false;
	
}
if ($type == 3)		
{			
	if ($sheet->getCell("A1") != "Search terms" || $sheet->getCell("B1") != "Organic" || $sheet->getCell("C1") != "Paid" || $sheet->getCell("D1") != "Traffic share" || $sheet->getCell("E1") != "Change")
		$checker = false;
	if ($sheet->getCell("A2")  == "" || $sheet->getCell("B2") == "" || $sheet->getCell("C2")  == "" || $sheet->getCell("D2")  == "" || $sheet->getCell("E2") == "")
		$checker = false;
}


