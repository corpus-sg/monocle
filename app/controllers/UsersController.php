<?php

class UsersController extends BaseController {
/**
* Display a listing of the resource.
*
* @return Response
*/
	protected $layout = "layouts.main";	
	public function login()
	{
		$email = Input::get('email');
		$pwd = Input::get('password');
		$count = DB::table('users')
                     ->where('email',$email)
					 ->where('status',1)
                     ->count();
		if ($count > 0)
		{
			$hashedPassword = DB::table('users')->where('email', $email)->first();
			$password = Hash::make($pwd);
			if (Input::get('rememberme') === 'yes') {
				if (Auth::attempt(array('email' => $email, 'password' => $pwd),true))
				{
					if (Auth::check())	
					{	
						if (Auth::user()->role == 1)
							return Redirect::to('listaccounts');
						else
							return Redirect::to('/');
					}
				}
				else
					return Redirect::to('/login')->with('wronglogin','Wrong Email or Password!');
			}
			else
			{
				if (Auth::attempt(array('email' => $email, 'password' => $pwd),false))
				{
					if (Auth::check())	
					{	
						if (Auth::user()->role == 1)
							return Redirect::to('listaccounts');
						else
							return Redirect::to('/');
					}
				}
				else
					return Redirect::to('/login')->with('wronglogin','Wrong Email or Password!');
			}
			
		}
		else
		{
			return Redirect::to('/login')->with('wronglogin','Wrong Email or Password!');
		}
	}
	
	public function show_account()
	{
		$this->layout->content =  View::make('users.showaccount');	
	}
	public function show_login()
	{
		$this->layout->content =  View::make('users.login');	
	}
	public function show_forgot_password()
	{
		$this->layout->content =  View::make('users.forgotpassword');
	}
	public function show_change_password()
	{
		return View::make('users.changepassword');
	}
	public function change_password()
	{
		$oldpassword = Input::get('oldpassword');
		$newpassword = Input::get('password');
		$cfmpassword = Input::get('cfmpassword');
		
		$currpassword = UserEntry::where('id','=',Auth::user()->id)->pluck('password');
		if (Hash::check($oldpassword, $currpassword))
		{
			if ($newpassword == $cfmpassword)
			{
				$hashedpassword = Hash::make($newpassword);
				UserEntry::where('id','=',Auth::user()->id)->update(array(
					'password'=>$hashedpassword
				));
				return Redirect::to('account')->with('message','Your password has been successfully changed.');
			}
			return Redirect::to('account')->with('message','<p  style="color:#FF0000;">Your passwords does not match.</p>');
		}
				return Redirect::to('account')->with('message','<p style="color:#FF0000;">You entered the wrong current password.</p>');
	}
	public function forgot_password()
	{
		$email = Input::get('email');
		$newpassword = str_random(8);
		$hashedpassword = Hash::make($newpassword);
		UserEntry::where('email','=',$email)->update(array(
			'password'=>$hashedpassword
		));
		$user = UserEntry::where('email','=',$email)->first();
		$data = array('email'=>$email,'password'=>$newpassword,'name'=>$user->first_name." ".$user->last_name);
		Mail::send('emails.forgotpassword', $data, function($message) use ($data)
		{	
			$message->to($data['email'])->subject('Forgot your password? Let\'s reset it.');
		});
		return Redirect::to('login')->with('message','Your new password has been mailed to you');
	}
	public function checkemail()
	{
		if (Auth::check())
			$count = UserEntry::where('email','=',Input::get('email'))->where('id','!=',Auth::user()->id)->count();
		else
			$count = UserEntry::where('email','=',Input::get('email'))->count();
			
		return Response::json($count);
	}
	public function register()
	{
		$rules = array(
			'email'         => 'required|email|unique:users',
			'password'      	=> 'required',
			'cfmpassword'		=> 'required|same:password',
			'firstname'         => 'required',
			'lastname'      	=> 'required',
			'company'           => 'required',
			'jobfunc'      		=> 'required',
			'country'      		=> 'required'
		);
		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) 
		{
			return Redirect::to('/register')
				->withErrors($validator);
		} 
		
		else 
		{
			$email = Input::get('email');
			$password = Input::get('password');
			$oldPwd = $password;
			$firstname = Input::get('firstname');
			$lastname = Input::get('lastname');
			$company = Input::get('company');
			$jobfunc = Input::get('jobfunc');
			$country = Input::get('country');
			$password = Hash::make($password);
			if (Input::get('updates') === '1') 
				$updates = 1;
			else
				$updates = 0;
			date_default_timezone_set('Singapore');
			$timezone = date('Y-m-d H:i:s', time());
			
			
			DB::table('users')->insert(array
				('email' => $email, 'first_name'=>$firstname,'last_name'=>$lastname,'company'=>$company,'job_function'=>$jobfunc,'password' => $password,'country'=>$country,'updates'=>$updates,'created_at'=>$timezone,'role'=>'2','status'=>'1')
			);
			
			$data = array('email'=>$email,'name'=>$firstname." ".$lastname);
			Mail::send('emails.createaccount', $data, function($message) use ($data)
			{	
				$message->to($data['email'])->subject('Welcome to Monocle!');
			});
			if (!Auth::check())
			{
				if (Auth::attempt(array('email' => $email, 'password' => $oldPwd),true))
				{
					if (Auth::check())		
						return Redirect::to('/');
					else
						return Redirect::to('/login');
				}
			}
			else
			{
				return Redirect::to('/listaccounts');
			}
		}
	}
	public function edit_account()
	{
		$rules = array(
			'email'         => 'required|email',
			'firstname'         => 'required',
			'lastname'      	=> 'required',
			'company'           => 'required',
			'jobfunc'      		=> 'required',
			'country'      		=> 'required'
		);
		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) 
		{
			return Redirect::to('/account')
				->withErrors($validator);
		} 
		
		else 
		{
			$id = Auth::user()->id;
			$email = Input::get('email');
			$password = Input::get('password');
			$oldPwd = $password;
			$firstname = Input::get('firstname');
			$lastname = Input::get('lastname');
			$company = Input::get('company');
			$jobfunc = Input::get('jobfunc');
			$country = Input::get('country');
			
			date_default_timezone_set('Singapore');
			$timezone = date('Y-m-d H:i:s a', time());
			
			
			DB::table('users')->where('id','=',$id)->update(array
				('email' => $email, 'first_name'=>$firstname,'last_name'=>$lastname,'company'=>$company,'job_function'=>$jobfunc,'country'=>$country,'updated_by'=>Auth::user()->id,'updated_at'=>$timezone)
			);
			
			return Redirect::to('account');
		}
	}
	public function show_register()
	{
		$this->layout->content =  View::make('users.register');	
	}

}
?>