<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoogleFilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('google_files', function($table) {
			$table->increments('id');
			$table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
			$table->softDeletes();
			$table->string('unique_word');
			$table->integer('frequency');
			$table->integer('character_count');
			$table->integer('summed_clicks');
			$table->integer('summed_impression');
			$table->decimal('summed_ctr', 20,15);
			$table->decimal('summed_avg_position', 20, 15);
			$table->integer('file_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('google_files');
	}

}
