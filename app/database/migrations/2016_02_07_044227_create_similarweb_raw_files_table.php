<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimilarwebRawFilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('similarweb_raw_files', function($table) {
			$table->increments('id');
			$table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
			$table->softDeletes();
			$table->text('search_terms');
			$table->decimal('organic', 20, 15);
			$table->decimal('paid', 20, 15);
			$table->decimal('traffic_share', 20, 15);
			$table->decimal('change', 20, 15);
			$table->decimal('volume', 20, 15);
			$table->decimal('cpc', 10, 2);
			$table->integer('file_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('similarweb_raw_files');
	}

}
