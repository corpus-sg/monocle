<?php

class UserTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		$user = new User;
		$user->first_name='Kris';
		$user->last_name='Kong';
		$user->email='kris@corpus.sg';
		$user->password=Hash::make('password');
		$user->company = 'Corpus Pte. Ltd.';
		$user->job_function='CEO';
		$user->updates=0;
		$user->updated_by=0;
		$user->country='Singapore';
		$user->role=1;
		$user->status=1;
		$user->remember_token='';
		$user->save(); 
	}

}
