<?php

use NinetySixSkies\Monocle\Handlers\NormalHandler;
use NinetySixSkies\Monocle\Handlers\SimilarWebHandler;
use NinetySixSkies\Monocle\Handlers\GoogleHandler;

class JobHandler
{

 	public function fire($job, $data)
    {
        ini_set('memory_limit', '1024M');
        $job->delete();
    	$handler = null;

    	switch ($data['type']) {
    		case '2':
    			$handler = new GoogleHandler($data);
    			break;

    		case '3':
    			$handler = new SimilarWebHandler($data);
    			break;

    		default:
    			$handler = new NormalHandler($data);
    			break;
    	}

    	$status = $handler->handle();
    	// if ($status == 'Completed') {
    	// 	$job->delete();
    	// }
    }
 }
