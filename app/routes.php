<?php

// Dashboard & Files
Route::get('/',array('uses'=>'CoverController@index'));
Route::get('/showfiles',array('before' => 'auth','as' => 'cover.show_files', 'uses'=>'CoverController@show_files'));
Route::get('/logout',array('uses'=>'CoverController@logout'));

// Admin functions
Route::get('/listaccounts',array('before' => 'auth','uses'=>'AdminController@show_accounts'));
Route::post('/updatesignup',array('before' => 'auth','uses'=>'AdminController@updatesignup'));
Route::post('/changestatus',array('before' => 'auth','uses'=>'AdminController@changestatus'));
Route::get('/createaccount',array('before' => 'auth','uses'=>'AdminController@show_createaccount'));
Route::post('/createaccount',array('before' => 'auth','uses'=>'AdminController@createaccount'));
Route::post('/deleteuser',array('before' => 'auth','uses'=>'AdminController@delete_user'));

// Basic user functions
Route::get('/login',array('uses'=>'UsersController@show_login'));
Route::post('/login',array('uses'=>'UsersController@login'));
Route::get('/register',array('uses'=>'UsersController@show_register'));
Route::post('/register',array('uses'=>'UsersController@register'));
Route::get('/forgot_password',array('uses'=>'UsersController@show_forgot_password'));
Route::post('/forgot_password',array('uses'=>'UsersController@forgot_password'));
Route::get('/account',array('before' => 'auth','uses'=>'UsersController@show_account'));
Route::post('/account',array('before' => 'auth','uses'=>'UsersController@edit_account'));
Route::get('/changepassword',array('uses'=>'UsersController@show_change_password'));
Route::post('/changepassword',array('uses'=>'UsersController@change_password'));
Route::post('/checkemail',array('uses'=>'UsersController@checkemail'));

// File Processing
Route::put('/upload',array('before' => 'auth','uses'=>'ProcessController@upload'));
Route::get('/upload',array('before' => 'auth','uses'=>'ProcessController@upload'));
Route::post('/upload',array('before' => 'auth','as' => 'process.upload', 'uses'=>'ProcessController@process_upload'));
Route::post('/uploadAction',array('before' => 'auth','uses'=>'ProcessController@process_upload'));
Route::post('/delete_file',array('before' => 'auth','uses'=>'ProcessController@delete_file'));
Route::post('/start_job',array('before' => 'auth','uses'=>'ProcessController@start_job'));
Route::get('download/{id}/{type}',array('before' => 'auth','uses'=>'ProcessController@download'));

// Testing
Route::get('/phpinfo', array('uses'=>'HomeController@phpInfo'));
Route::get('/reset', array('uses' => 'HomeController@reset'));


/*
Route::get('/files',array('as' => 'process.show_files', 'uses'=>'ProcessController@show_files'));
Route::post('/ajaxUserProfile',array('uses'=>'HomeController@ajaxUserProfile'));
Route::post('/ajaxInProgress',array('uses'=>'CoverController@ajaxInProgress'));
Route::get('/ajaxInProgress',array('as' => 'cover.ajaxInProgress', 'uses'=>'CoverController@ajaxInProgress'));

Route::get('/attachments', array('uses'=>'AttachmentsController@index'));

Route::resource('attachments', 'File\AttachmentsController',array('names' => array('index'  => 'file.attachments.index'
                                                                                            ,'create' =>'file.attachments.create'
                                                                                            ,'store'  =>'file.attachments.store'
                                                                                            ,'show'   =>'file.attachments.show'
                                                                                            ,'edit'   =>'file.attachments.edit'
                                                                                            ,'update' =>'file.attachments.update'
                                                                                            ,'destroy'=>'file.attachments.destroy'
                                                                                            )));

Route::resource('attachments', 'AttachmentsController',array('names' => array('index'  => 'attachments.index'
                                                                                            ,'create' =>'attachments.create'
                                                                                            ,'store'  =>'attachments.store'
                                                                                            ,'show'   =>'attachments.show'
                                                                                            ,'edit'   =>'attachments.edit'
                                                                                            ,'update' =>'attachments.update'
                                                                                            ,'destroy'=>'attachments.destroy'
                                                                                            )));
                                                                                            */