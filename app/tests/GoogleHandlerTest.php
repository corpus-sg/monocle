<?php

class GoogleHandlerTest extends TestCase {

	/**
	 * Creates the application.
	 *
	 * @return \Symfony\Component\HttpKernel\HttpKernelInterface
	 */
	public function testGetTmpName()
	{
		$file = new FileEntry;
		$file->file_name = 'Item [1] GSC_Search Analytics_20160204234609.csv';

		$this->assertEquals($file->getTmpName(), 'Testing');
	}

}
