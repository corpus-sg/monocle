<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>SEO Sheet Processor</title>

  </head>

  <body>

  <section id="container" >
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<div class="col-md-12">
{{ Form::open(array("action"=>"UsersController@register")) }}
    <div class=" " style="min-width:600px;padding:10px 30px;">
    	<div class="createnewunderline"><h3 class="nomargin">Create New Account</h3></div>
        <div class="input">
            <div class="legend">Email Address</div>
            <div class="textinput ">
            	<input type="email" id="emailinput" name="email" class="form-control">
            	<div class="errormsg" id="emailmsg">Please fill in your email address.</div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="input">
            <div class="legend">Password</div>
            <div class="textinput">
            	<input type="password" id="pwdinput" name="password" class="form-control">
            	<div class="errormsg" id="pwdmsg">Please fill in your password.</div>
            </div>
            <div class="clear"></div>
        </div> 
        <div class="input">
            <div class="legend">Confirm Password </div>
            <div class="textinput">
            	<input type="password" id="cfmpwdinput" name="cfmpassword" class="form-control">
            	<div class="errormsg" id="cfmpwdmsg">Please confirm your password.</div>
            </div>
            <div class="clear"></div>
        </div> 
        <div class="input">
            <div class="legend">First Name</div>
            <div class="textinput">
            	<input type="text" id="firstnameinput" name="firstname" class="form-control">
            	<div class="errormsg" id="firstnamemsg">Please fill in your first name.</div>
            </div>
            <div class="clear"></div>
        </div> 
        <div class="input">
            <div class="legend">Last Name</div>
            <div class="textinput">
            	<input type="text" id="lastnameinput" name="lastname" class="form-control">
            	<div class="errormsg" id="lastnamemsg">Please fill in your last name.</div>
            </div>
            <div class="clear"></div>
        </div> 
        <div class="input">
            <div class="legend">Company</div>
            <div class="textinput">
            	<input type="text" id="companyinput" name="company" class="form-control">
            	<div class="errormsg" id="companymsg">Please fill in your company.</div>
            </div>
            <div class="clear"></div>
        </div> 
        <div class="input">
            <div class="legend">Job Function</div>
            <div class="textinput">
            	<select name="jobfunc" class="form-control">
				<?php
					$countries = JobFunctionEntry::orderBy('name')->get();
					foreach ($countries as $key=>$value)
					{
						?>
                        <option value="{{ $value->name }}">{{ $value->name }}</option>
                        <?php
					}
				?>
                </select>
            	<div class="errormsg" id="jobfuncmsg">Please fill in your job function.</div>
             </div>
            <div class="clear"></div>
        </div> 
        <div class="input padbot">
            <div class="legend">Country</div>
            <div class="textinput">
            	<select name="country" class="form-control">
				<?php
					$countries = CountryEntry::orderBy('name')->get();
					foreach ($countries as $key=>$value)
					{
						?>
                        <option @if ($value->name == "Singapore") selected @endif value="{{ $value->name }}">{{ $value->name }}</option>
                        <?php
					}
				?>
                </select>
            </div>
            <div class="clear"></div>
        </div> 
        <div class="clear"></div>
        <div class="right">
        	<div class="rememberme"><input type="checkbox" name="updates" value="1" id="updates"> <label for="updates">Sign Up for Updates</label></div>
            <div class="login"><input type="submit" id="submitbtn" class="btn btn-warning" value="Sign Up"></div>
        </div>
        <div class="clear"></div>
    </div>
{{ Form::close() }}
</div>

</section>
 
 
<script>
	var checkemail = false,
		checkpwd = false,
		checkcfmpwd = false,
		checkfirstname = false,
		checklastname = false,
		checkcompany = false,
		checkjobfunc = false;
	
	function checkEmail()
	{
		if ($("#emailinput").val() == "")
		{
			$("#emailmsg").html("Please fill in your email address.");
			$("#emailmsg").animate({
				opacity:1
			},300);
		}
		else if(!isValidEmailAddress($("#emailinput").val())) 
		{
			$("#emailmsg").html("Your email address is invalid. Please fill in the correct email address.");
			$("#emailmsg").animate({
				opacity:1
			},300);
		}
		else
		{
			$("#emailmsg").animate({
				opacity:0
			},100);
			
			checkemail = true;
			$.ajax({
					type: 'post',
					url: './checkemail',
					cache: false,
					dataType: 'json',
					data: 'email='+$("#emailinput").val(),
					beforeSend: function() { 
					},
					success: function(data) {
						if(data.success == false)
						{  
						} 
						else
						{
							if (data > 0)
							{
								$("#emailmsg").html("This email address is already registered. Please choose another email address.");
								$("#emailmsg").animate({
									opacity:1
								},100);
								checkemail = false;
							}
						}
					},
					error: function(xhr, textStatus, thrownError) {
					}
			});
			
		}
	}
	function checkPwd()
	{
		if ($("#pwdinput").val() == "")
		{
			$("#pwdmsg").animate({
				opacity:1
			},300);
		}
		else
		{
			$("#pwdmsg").animate({
				opacity:0
			},100);
			checkpwd = true;
		}
	}
	function checkCfmPwd()
	{
		if ($("#cfmpwdinput").val() == "")
		{
			$("#cfmpwdmsg").html("Please confirm in your password.");
			$("#cfmpwdmsg").animate({
				opacity:1
			},300);
		}
		else if ($("#cfmpwdinput").val() != $("#pwdinput").val())
		{
			$("#cfmpwdmsg").html("Your entered password is not the same. Please enter the correct password.");
			$("#cfmpwdmsg").animate({
				opacity:1
			},300);
		}
		else
		{
			$("#cfmpwdmsg").animate({
				opacity:0
			},100);
			checkcfmpwd = true;
		}
	}
	function checkFirstName()
	{
		if ($("#firstnameinput").val() == "")
		{
			$("#firstnamemsg").animate({
				opacity:1
			},300);
		}
		else
		{
			$("#firstnamemsg").animate({
				opacity:0
			},100);
			checkfirstname = true;
		}
	}
	function checkLastName()
	{
		if ($("#lastnameinput").val() == "")
		{
			$("#lastnamemsg").animate({
				opacity:1
			},300);
		}
		else
		{
			$("#lastnamemsg").animate({
				opacity:0
			},100);
			checklastname = true;
		}
	}
	function checkCompany()
	{
		if ($("#companyinput").val() == "")
		{
			$("#companymsg").animate({
				opacity:1
			},300);
		}
		else
		{
			$("#companymsg").animate({
				opacity:0
			},100);
			checkcompany = true;
		}
	}
	function checkJobFunc()
	{
		if ($("#jobfuncinput").val() == "")
		{
			$("#jobfuncmsg").animate({
				opacity:1
			},300);
		}
		else
		{
			$("#jobfuncmsg").animate({
				opacity:0
			},100);
			checkjobfunc = true;
		}
	}
	$("#emailinput").focusout(function(){
		checkEmail();
	});
	$("#pwdinput").focusout(function(){
		checkPwd();
	});
	$("#cfmpwdinput").focusout(function(){
		checkCfmPwd();
	});
	$("#firstnameinput").focusout(function(){
		checkFirstName();
	});
	$("#lastnameinput").focusout(function(){
		checkLastName();
	});
	$("#companyinput").focusout(function(){
		checkCompany();
	});
	function isValidEmailAddress(emailAddress) {
		var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
		return pattern.test(emailAddress);
	};
	$("form").submit(function(){
		if (checkemail == true && checkpwd == true && checkcfmpwd == true && checkfirstname == true && checklastname == true && checkcompany == true)
		{
			$("#submitbtn").prop('disabled',true);
			return true;
		}
		else
		{
			
			checkEmail();
			checkPwd();
			checkCfmPwd();
			checkFirstName();
			checkLastName();
			checkCompany();
			
			$(window).scrollTop($('form').offset().top);
			return false;
		}
	});
</script>
</body>
</html>