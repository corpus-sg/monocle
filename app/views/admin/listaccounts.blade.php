@extends('layouts.main')

@section('header')
{{HTML::style('fancybox/jquery.fancybox-1.3.4.css') }}
 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@stop
@section('content')
<div class="col-md-12">
    <!-- -- BASIC PROGRESS BARS ---->
    <h3><i class="fa fa-angle-right"></i> Manage Accounts</h3>
   
  	<div class="showback">
    	<h4><i class="fa fa-angle-right"></i> User Accounts Administration <a class="fancybox" id="createaccount" href="{{URL::to('createaccount')}}"><input type="button" class="btn btn-primary" value="+ Create New Account" ></a></h4>
       
         <table class="table table-bordered table-striped bootstrap-datatable smallerfont datatable">
                  <thead>
                      <tr>
                          <th>Customer ID</th>
                          <th>Email Address</th>
                          <th class="onlydesktop">First Name</th>
                          <th class="onlydesktop">Last Name</th>
                          <th class="onlydesktop">Company</th>
                          <th class="onlydesktop">Job Function</th>
                          <th class="onlydesktop">Country</th>
                          <th class="onlydesktop">Updates</th>
                          <th>Actions</th>
                      </tr>
                     
                  </thead>   
                  <tbody>
                  @foreach($users as $key=>$value)
                      <tr>
                      	   <td>
                           	<?php
								echo "U";
								$zeroes = 6 - strlen($value->id);
								for ($m = 0; $m < $zeroes;$m++)
									echo "0";
								echo $value->id;
							?>
                           </td>
                      	   <td>{{ $value->email }}</td>
                      	   <td class="onlydesktop">{{ $value->first_name }}</td>
                      	   <td class="onlydesktop">{{ $value->last_name }}</td>
                      	   <td class="onlydesktop">{{ $value->company }}</td>
                      	   <td class="onlydesktop">{{ $value->job_function }}</td>
                      	   <td class="onlydesktop">{{ $value->country }}</td>
                      	   <td class="onlydesktop">
                           		<span class="invis">{{ $value->updates }}</span>
                           		<input type="checkbox" class="updates" onChange="changeupdates({{$value->id}})" name="updates" @if ($value->updates == 1) checked @endif>
                           
                           </td>
                      	   <td>
                           <?php
						   		if ($value->status == 1)
						   			$btn = "Deactivate";
								else
									$btn = "Activate";
									
						   ?>
                           	{{ Form::open(array('action'=>'AdminController@delete_user','class'=>'formdel','id'=>$value->id)) }}
                            <input type="hidden" value="{{ $value->id }}" name="id">
                           		<input type="button" id="btn{{$value->id}}" onClick="changestatus({{$value->id}})" value="{{$btn}}" class="btn btn-primary btn-xs">
                                <input type="submit" value="Delete" class="btn btn-danger btn-xs">
                            {{ Form::close() }}
                           </td>
                      </tr>
                      @endforeach
                  </tbody>
         </table>
    </div>
</div>
@stop

@section ('script')
{{ HTML::script('/fancybox/jquery.fancybox-1.3.4.js') }}
<script>
$(document).ready( function () {
	$(".datatable").DataTable({ "order": [[ 0, "asc" ]]});
	$("#createaccount").fancybox({'width':'700px',
                         'height':'80%','autoSize':false});
});
function changeupdates(id){
	$.ajax({
			type: 'post',
			url: './updatesignup',
			cache: false,
			dataType: 'json',
			data: 'id='+id,
			beforeSend: function() { 
			},
			success: function(data) {
				if(data.success == false)
				{  
				} 
				else
				{					
				}
			},
			error: function(xhr, textStatus, thrownError) {
			}
	});
}
function changestatus(id){
	if ($("#btn"+id).val() == "Activate")
		$("#btn"+id).val("Deactivate");
	else
		$("#btn"+id).val("Activate");
	$.ajax({
			type: 'post',
			url: './changestatus',
			cache: false,
			dataType: 'json',
			data: 'id='+id,
			beforeSend: function() { 
			},
			success: function(data) {
				if(data.success == false)
				{  
				} 
				else
				{					
				}
			},
			error: function(xhr, textStatus, thrownError) {
			}
	});
}
var no = 0;
$(".formdel").submit(function(ev) {
	if (no == 0)
	{
		var id = $(this).attr('id');
	
		Boxy.confirm("Are you sure?", function() { no = 1; $("#"+id).submit(); }, {title: 'Confirm'});
		return false;
	}
});
</script>
@stop