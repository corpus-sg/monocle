<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Forgot Password</title>
</head>

<body>

<div style="width:600px; border:solid 1px #ccc; font:normal 10pt Helvetica;">
	<div style="width:600px; height:65px; background:#333; border-bottom:solid 1px #ccc;">
		<img src="{{URL::to('img/logo.png')}}" alt="Monocle" title="Monocle" border="0" style="margin:10px 20px;">
	</div>
	<div style="width:600px; background:#fff;">
		<div style="padding:25px 20px 35px 20px;">
			<p>Hello {{ $name }},</p>
			<p>Forgot your password? It's time for you to can come up with an even better one.</p>
			<p>New Password : <b>{{ $password }}</b></p>
			<p style="margin:30px 0;"><a href="http://monocle.tinkeredge.com/login" style="background:#40a6d9; padding:10px 20px; text-decoration:none; font-weight:bold; color:#fff; border:solid 1px #999;">Login</a></p>
			
			<p>Cheers
			<br>TinkerEdge</p>
		</div>
	</div>
	<div style="width:600px; background:#eee; border-top:solid 1px #ccc; font-size:8pt;">
		<div style="padding:20px 20px;">
			Copyright &copy; 2012 - {{ date('Y') }}. 96 Skies. All Rights Reserved.
		</div>
	</div>
</div>


</body>
</html>
