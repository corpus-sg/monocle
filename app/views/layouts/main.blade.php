<?php
	ini_set("memory_limit","7G");
	ini_set('max_execution_time', '0');
	ini_set('max_input_time', '0');
	set_time_limit(0);
	ignore_user_abort(true);  

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Monocle, TinkerEdge, SEO">

    <title>Monocle App by TinkerEdge</title>

    <!-- Bootstrap core CSS -->
    {{ HTML::style('css/main.css') }}

    <!--external css-->
    {{ HTML::style('css/bootstrap.min.css') }}
    {{ HTML::style('font-awesome/css/font-awesome.css') }}

    <!-- blueimp Gallery styles -->
    {{ HTML::style('css/blueimp-gallery.min.css') }}

    <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
    {{ HTML::style('css/jquery.fileupload.css') }}
    {{ HTML::style('css/jquery.fileupload-ui.css') }}

    <!-- {{ HTML::style('js/bootstrap-datepicker/css/datepicker.css') }} -->
    <!-- {{ HTML::style('assets/js/bootstrap-daterangepicker/daterangepicker.css') }} -->
        
    <!-- Custom styles for this template -->
    {{ HTML::style('css/style.css') }}
    {{ HTML::style('css/style-responsive.css') }}
	  {{ HTML::style('src/css/boxy.css'); }}

    <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->

    {{ HTML::style('css/jquery.dataTables.min.css') }}
	    {{-- {{ HTML::style('assets/css/boxy.css'); }} --}}
    @yield('header')

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') }}
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js') }}
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
            <!--logo start-->
            <a href="{{ URL::to('/') }}" class="logo">{{ HTML::image('img/logo.png') }}</a>
            <!--logo end-->
            <div class="nav notify-row" id="top_menu">
                <!--  notification start -->
               @if (Auth::user())
            	<ul class="nav top-menu">
                    <li><a href="{{ URL::to('/') }}">Dashboard</a></li>
                    @if (Auth::user()->role == 1)
                    	<li><a href="{{ URL::to('/listaccounts') }}">Accounts</a></li>
					@endif
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
                            Job Options
                        </a>
                    <ul class="dropdown-menu extended inbox" style="left:0;">
                            <div class="notify-arrow notify-arrow-green" style="left:7px;"></div>
                            <li>
                                <a id="normalupload">
                                  Excel
                                </a>
                            </li>
                            <li>
                                <a id="googleupload">
                                  Google Search Console
                                </a>
                            </li>
                            <li>
                                <a id="similiarwebupload">
                                  SimiliarWeb
                                </a>
                            </li>
                            
                            <li>
                                <a href="{{ URL::to('showfiles') }}">
                                  Completed Jobs
                                </a>
                            </li>
                        </ul>
                    </li>
            	</ul>
                @endif
                <!--  notification end -->
            </div>
          	<div class="nav pull-right top-menu">
            <ul class="nav top-menu" style="padding-top:15px;">
                    @if (Auth::user())
                    <!-- menu dropdown start-->
                    <li id="header_inbox_bar" class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
                            <i class="fa fa-cog" style="font-size:20px;"></i>
                        </a>
                        <ul class="dropdown-menu extended inbox">
                            <div class="notify-arrow notify-arrow-green"></div>
                             <li>
                                <a href="{{ URL::to('account') }}">
                                  Account Details
                                </a>
                            </li>
                            <li>
                                <a href="{{ URL::to('logout') }}">
                                  Logout
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- menu dropdown end -->
                    @else
                    <li><a href="{{ URL::to('/login') }}">Login</a></li>
                    <li><div style="color:#FFF;font-weight: 700;padding-top: 2px;font-size:1.4em;padding-right: 7px;cursor:default;"> | </div></li>
                    <li><a href="{{ URL::to('/register') }}">Register</a></li>
                    
                    @endif
                </ul>
            </div>
        </header>
      <!--header end-->
      

   
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
      	<section id="wrapper">
      		@yield('content')
		</section>
        
      </section>
      <div style="clear:both"></div>
        <footer class="site-footer">
          <div class="text-center">
             	 © {{ date('Y') }} Monocle App by Tinker Edge
            
          </div>
    	  </footer>
      <!--main content end-->
    	
    {{ Form::open(array('action'=>'ProcessController@upload','id'=>'uploadform','method'=>'put')) }}
        {{ Form::hidden('id',0,array('id'=>'processid')) }}
    {{ Form::close() }}
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <!-- {{ HTML::script('js/vendor/user_profile.js') }} -->

    {{ HTML::script('js/jquery-1.11.1.min.js') }}
    {{ HTML::script('js/bootstrap.min.js') }}
    {{ HTML::script('js/jquery.dcjqaccordion.2.7.js') }}
    {{ HTML::script('js/jquery.scrollTo.min.js') }}
    {{ HTML::script('js/jquery.nicescroll.js') }}


    <!--common script for all pages-->
    {{ HTML::script('js/common-scripts.js') }}

    <!--script for this page-->
    {{ HTML::script('js/jquery-ui-1.9.2.custom.min.js') }}

	<!--custom switch-->
	{{ HTML::script('js/bootstrap-switch.js') }}
	
	<!--custom tagsinput-->
	{{ HTML::script('js/jquery.tagsinput.js') }}
	
	<!--custom checkbox & radio-->
	
	<!-- {{ HTML::script('js/bootstrap-datepicker/js/bootstrap-datepicker.js') }} -->
	<!-- {{ HTML::script('js/bootstrap-daterangepicker/date.js') }} -->
	<!-- {{ HTML::script('js/bootstrap-daterangepicker/daterangepicker.js') }} -->
	
	{{ HTML::script('js/bootstrap-inputmask/bootstrap-inputmask.min.js') }}
	
	
	{{ HTML::script('js/form-component.js') }}   
    
  {{ HTML::script("src/js/jquery.boxy.js") }}
  {{ HTML::script("js/jquery.dataTables.min.js") }}
  <script type="text/javascript">
    function setUserId() {
      var user_profile = JSON.parse(localStorage["user_profile"]);
      var user_id = "{{ $user_id or 0}}";
      user_profile.user_id = user_id;
      localStorage.setItem('user_profile', JSON.stringify(user_profile));
    }
	$("#normalupload").click(function(){
		$("#processid").val(1);
		$("#uploadform").submit();
	});
	$("#googleupload").click(function(){
		$("#processid").val(2);
		$("#uploadform").submit();
	});
	$("#similiarwebupload").click(function(){
		$("#processid").val(3);
		$("#uploadform").submit();
	});
  </script>
    @yield('script')
    

  </body>
</html>
