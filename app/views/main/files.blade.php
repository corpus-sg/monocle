@extends('layouts.main')

@section('header')

 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@stop
@section('content')
<div class="col-md-12">
    <!-- -- BASIC PROGRESS BARS ---->
    <h3><i class="fa fa-angle-right"></i> Completed Jobs</h3>
    	<div class="showback">
    	<!--<h4><i class="fa fa-angle-right"></i> Completed Jobs</h4>-->
       
         <table class="table table-bordered table-striped bootstrap-datatable smallerfont datatable">
                  <thead>
                      <tr>
                          <th style="width:30%">File Name</th>
                          <th>Job Option Type</th>
                          <th class="onlydesktop">Job Start</th>
                          <th class="onlydesktop">Job End</th>
                          <th class="onlydesktop">Completion</th>
                          <th>Actions</th>
                      </tr>
                  </thead>   
                  <tbody>
                  
                  @foreach ($finishedfiles as $key=>$value)
                  	<tr id="row{{ $value->id }}">
                    	<td>{{ $value->file_name }}</td>
                    	<td>
                        	@if ($value->type == 1)
                            	<?php $folder = "normal"; ?>
                            	Normal 
                            @elseif ($value->type == 2)
                            	<?php $folder = "gwt"; ?>
                            	Google Webmaster
                            @else
                            	<?php $folder = "similiarweb"; ?>
                            	Similiar Web
                            @endif
                        </td>
						<?php
                            $tmpname = explode('.',$value->file_name);
                            $tmpname[0] = str_replace(' ', '', $tmpname[0]);
                        ?>
                    	<td class="onlydesktop"><span class="none">{{ date ('YmdHis',strtotime($value->started_at)) }}</span>{{ date('d-m-Y H:i:s',strtotime($value->started_at)) }}</td>
                    	<td class="onlydesktop" id="{{ $tmpname[0] }}date">
                        	@if ($value->finished_at != "0000-00-00 00:00:00")
                            	<span class="none">{{ date ('YmdHis',strtotime($value->finished_at)) }}</span>
	                        	{{ date('d-m-Y H:i:s',strtotime($value->finished_at)) }}
                                <?php $tmpname[0] = ""; ?>
 							@else
                            	---
                            @endif
                        </td>
                    	<td class="onlydesktop" id="{{ $tmpname[0] }}cell">
                        	@if ($value->finished_at != "0000-00-00 00:00:00")
                            	Completed
 							@else
                            	
                                <div class="progress progress-striped active" id="{{ $tmpname[0] }}progress">
                                  <div class="progress-bar" role="progressbar" id="{{ $tmpname[0] }}bar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 1%">
                                    <span class="sr-only">0% Complete</span>
                                  </div>
                                </div>
                            @endif
                        </td>
                    	<td>
							<?php
                                $name = explode('.',$value->file_name);
							?>
                       		@if ($value->finished_at != "0000-00-00 00:00:00")
                            	
                                {{ Form::open(array('action'=>'ProcessController@delete_file','method'=>'post','class'=>'deletefileform','id'=>$value->id)) }}
                                {{ Form::hidden('id',$value->id) }}
                                {{ Form::hidden('type',$value->type) }}
                                {{ Form::hidden('filename',$value->file_name) }}
                                <a href="<?php echo $folder."/".$name[0].'-Report.xlsx'; ?>" download><button type="button" class="btn btn-xlsx"><i class="fa "> &nbsp;&nbsp;&nbsp;</i></button></a>
                                <a href="<?php echo $folder."/".$name[0].'-Report.csv'; ?>" download><button type="button" class="btn btn-csv"><i class="fa "> &nbsp;&nbsp;&nbsp;</i></button></a>
                                <button type="submit" class="btn btn-delete"><i class="fa "> &nbsp;&nbsp;&nbsp;</i></button>
                                {{ Form::close() }}
                            @else
                            	<div class="{{ $tmpname[0] }}out">
                                {{ Form::open(array('action'=>'ProcessController@delete_file','method'=>'post','class'=>'deletefileform','id'=>$value->id)) }}
                                {{ Form::hidden('id',$value->id) }}
                                {{ Form::hidden('type',$value->type) }}
                                {{ Form::hidden('filename',$value->file_name) }}
                        		<button type="submit" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</button>
                                </div>
                                <div class="{{ $tmpname[0] }}in none">
                                <a href="<?php echo $folder."/".$name[0].'-Report.xlsx'; ?>" download><button type="button" class="btn btn-xlsx"><i class="fa "> &nbsp;&nbsp;&nbsp;</i></button></a>
                                <a href="<?php echo $folder."/".$name[0].'-Report.csv'; ?>" download><button type="button" class="btn btn-csv"><i class="fa "> &nbsp;&nbsp;&nbsp;</i></button></a>
 								<button type="submit" class="btn btn-delete"><i class="fa "> &nbsp;&nbsp;&nbsp;</i></button>
                                </div>
                                {{ Form::close() }}
                            @endif
                        </td>
                    </tr>

                 @endforeach
                  </tbody>
         </table>
    </div>
    <!--
  	<div class="showback">
    	<h4><i class="fa fa-angle-right"></i> Running Jobs</h4>
       
         <table class="table table-bordered table-striped bootstrap-datatable smallerfont datatable">
                  <thead>
                      <tr>
                          <th style="width:30%">File Name</th>
                          <th>Job Option Type</th>
                          <th class="onlydesktop">Job Start</th>
                          <th class="onlydesktop">Job End</th>
                          <th class="onlydesktop">Completion</th>
                          <th>Actions</th>
                      </tr>
                  </thead>   
                  <tbody>
                    <?php $filesname = ""; ?>
                  
                  @foreach ($processingfiles as $key=>$value)
                  	<tr id="row{{ $value->id }}">
                    	<td>{{ $value->file_name }}</td>
                    	<td>
                        	@if ($value->type == 1)
                            	<?php $folder = "normal"; ?>
                            	Normal 
                            @elseif ($value->type == 2)
                            	<?php $folder = "gwt"; ?>
                            	Google Webmaster
                            @else
                            	<?php $folder = "similiarweb"; ?>
                            	Similiar Web
                            @endif
                        </td>
						<?php
                            $tmpname = explode('.',$value->file_name);
                            $tmpname[0] = str_replace(' ', '', $tmpname[0]);
                        ?>
                    	<td class="onlydesktop"><span class="none">{{ date ('YmdHis',strtotime($value->started_at)) }}</span>{{ date('d-m-Y H:i:s',strtotime($value->started_at)) }}</td>
                    	<td class="onlydesktop" id="{{ $tmpname[0] }}date">
                        	@if ($value->finished_at != "0000-00-00 00:00:00")
                            	<span class="none">{{ date ('YmdHis',strtotime($value->finished_at)) }}</span>
	                        	{{ date('d-m-Y H:i:s',strtotime($value->finished_at)) }}
                                <?php $tmpname[0] = ""; ?>
 							@else
                            	---
                            @endif
                        </td>
                    	<td class="onlydesktop" id="{{ $tmpname[0] }}cell">
                        	@if ($value->finished_at != "0000-00-00 00:00:00")
                            	Completed
 							@else
                            	
                                <div class="progress progress-striped active" id="{{ $tmpname[0] }}progress">
                                  <div class="progress-bar" role="progressbar" id="{{ $tmpname[0] }}bar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 1%">
                                    <span class="sr-only">0% Complete</span>
                                  </div>
                                </div>
                                <?php
		                            $filesname .= $tmpname[0].".tmp;";
								?>
                            @endif
                        </td>
                    	<td>
							<?php
                                $name = explode('.',$value->file_name);
							?>
                       		@if ($value->finished_at != "0000-00-00 00:00:00")
                            	
                                {{ Form::open(array('action'=>'ProcessController@delete_file','method'=>'post','class'=>'deletefileform','id'=>$value->id)) }}
                                {{ Form::hidden('id',$value->id) }}
                                {{ Form::hidden('type',$value->type) }}
                                {{ Form::hidden('filename',$value->file_name) }}
                                <a href="<?php echo $folder."/".$name[0].'-Report.xlsx'; ?>" download><button type="button" class="btn btn-primary"><i class="fa fa-file-excel-o"></i></button></a>
                                <a href="<?php echo $folder."/".$name[0].'-Report.csv'; ?>" download><button type="button" class="btn btn-warning"><i class="fa fa-file-code-o"></i></button></a>
 								<button type="submit" class="btn btn-danger"><i class="fa fa-times"></i></button>
                                {{ Form::close() }}
                            @else
                            	<div class="{{ $tmpname[0] }}out">
                                {{ Form::open(array('action'=>'ProcessController@delete_file','method'=>'post','class'=>'deletefileform','id'=>$value->id)) }}
                                {{ Form::hidden('id',$value->id) }}
                                {{ Form::hidden('type',$value->type) }}
                                {{ Form::hidden('filename',$value->file_name) }}
                        		<button type="submit" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</button>
                                </div>
                                <div class="{{ $tmpname[0] }}in none">
                                <a href="<?php echo $folder."/".$name[0].'-Report.xlsx'; ?>" download><button type="button" class="btn btn-primary"><i class="fa fa-file-excel-o"></i></button></a>
                                <a href="<?php echo $folder."/".$name[0].'-Report.csv'; ?>" download><button type="button" class="btn btn-warning"><i class="fa fa-file-code-o"></i></button></a>
 								<button type="submit" class="btn btn-danger"><i class="fa fa-times"></i></button>
                                </div>
                                {{ Form::close() }}
                            @endif
                        </td>
                    </tr>

                 @endforeach
                  </tbody>
         </table>
    </div>
    -->
</div>

<input type="hidden" id="filesname" value="{{ $filesname }}">
{{ Form::open(array('action'=>'ProcessController@upload','id'=>'uploadform','method'=>'put')) }}
	{{ Form::hidden('id',0,array('id'=>'processid')) }}
{{ Form::close() }}

@stop

@section ('script')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script>
runscript();
window.setInterval(function(){runscript();}, 2500);
	function runscript(){
		
		var name = $("#filesname").val().split(';');
		for (var n = 0; n < name.length -1; n++)
		{
			var check = false;
			var tmpname = name[n].split('.');
			var request = jQuery.get('./tmp/'+name[n]+'?nocache=' + Date.now(), function(data) {
				//alert(name[n]);
				check = true;
				var tmp = data.split(';');
				$("#"+tmp[0]+"bar").css('width',tmp[1]+"%");
				if (tmp[1] == 100)
				{
					var currentdate = new Date(); 
					var datetime = "" + ( '0' + (currentdate.getDate()) ).slice( -2 ) + "-"
					+  ( '0' + (currentdate.getMonth()+1) ).slice( -2 )  + "-" 
					+ currentdate.getFullYear() + " "  
					+ ( '0' + (currentdate.getHours()) ).slice( -2 ) + ":"  
					+ ( '0' + (currentdate.getMinutes()) ).slice( -2 ) + ":" 
					+ ( '0' + (currentdate.getSeconds()) ).slice( -2 );
					$("#"+tmp[0]+"bar").css("width",100).delay(500).fadeOut('slow','swing',function(){
					
					$("#"+tmp[0]+"cell").html("Completed");
					$("#"+tmp[0]+"date").html(datetime);
					});
					//alert(tmpname[0]);
					$("."+tmp[0]+"out").delay(500).fadeOut('slow','swing',function(){
						$("."+tmp[0]+"in").fadeIn();
					});
				}
			});
			request.success(function(result) {
			  console.log(result);
			});
			
			request.error(function(jqXHR, textStatus, errorThrown) {
			  if (textStatus == 'timeout')
				alert('The server is not responding');
			
			  if (textStatus == 'error'){
				  if(errorThrown == "Not Found")
				  {
				  }
			  }
			  // Etc
			});
		}
	}
	var table = $('.datatable').DataTable({ "order": [[ 2, "desc" ]]});
	$("#normalsheet").click(function(){
		$("#processid").val(1);
		$("#uploadform").submit();
	});
	$("#googleweb").click(function(){
		$("#processid").val(2);
		$("#uploadform").submit();
	});
	$("#similiarweb").click(function(){
		$("#processid").val(3);
		$("#uploadform").submit();
	});
	function setUp(){
		$(".deletefileform").submit(function(){
			$.ajax({
					type: 'post',
					url: './delete_file',
					cache: false,
					dataType: 'json',
					data: $(this).serialize(),
					beforeSend: function() { 
					},
					success: function(data) {
						if(data.success == false)
						{  
							$('.horizontalLine').html('Something went to wrong.Please Try again later...');	
						} 
						
					},
					error: function(xhr, textStatus, thrownError) {
						$('.horizontalLine').html(thrownError);
						
					}
			});
			$("#row"+$(this).attr('id')).fadeOut('slow','swing',function(){
				
     	         	   table.row(this).remove().draw( false );
			   setUp();
			});
			return false;
		});
		$(".paginate_button").click(function() {
			setUp();
		});

		$("input[type=search]").blur(function(){
			setUp();
		});
		$("select").change(function(){
			setUp();
		});
	}
	setUp();
</script>
@stop