@extends('layouts.main')

    @section('header')
     <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css"> -->
    @stop
    @section('content')
    <div class="col-md-12">
        <!-- BASIC PROGRESS BARS -->
        <h3>

        @if ($id == 1)
            {{ HTML::image('img/normal.jpg', '', array('style'=>'height:85px;')) }}            
        @elseif ($id == 2)
            {{ HTML::image('img/googleweb.jpg', '', array('style'=>'height:85px;')) }}
        @else
            {{ HTML::image('img/similiarweb.jpg', '', array('style'=>'height:85px;')) }}
        @endif
	</h3>
        <div class="showback">
            <h4><i class="fa fa-angle-right"></i> 
            	@if ($id == 1)
                	Excel Job Option (Accepts files in .xls, .xlsx or .csv extension)
                @elseif ($id == 2)
                	Google Search Console (Accepts download files from Search Analytics > Queries Report)
                @else
                	SimilarWeb (Accepts exported reports from Traffic Sources > Search > Search Terms Report)
                @endif
            
            </h4>
            <div class="horizontalLine"></div>
            @include('include.delimiter', ['id' => $id])            
            @include('include.multi_upload', array('id'=>$id))       
            <!-- @include('include.upload_js_script') -->
            {{ Form::open(array('action'=>'ProcessController@start_job','id'=>'startjob')) }}
            <input type="hidden" name="processfiles" id="processfiles">
            <input type="hidden" name="type" value="{{ $id }}">
            <button type="submit" disabled class="btn btn-primary start starter">
                <i class="glyphicon glyphicon-upload"></i>
                <span>Start Job</span>
            </button>
           
        	{{ Form::close() }}
        </div><!--/showback -->
    </div><!-- /end col-md-12 -->

    @stop <!-- end content-->
    
    @section('script')         
            @include('include.upload_script')
	<script>
		var name= "",idx=0;
		$("#startjob").submit(function(e){
			idx = 0;
			$('.name a').each(function() {
			
				$("#processfiles").val($(this).html());
				$.ajax({
						type: 'post',
						url: './start_job',
						cache: false,
						dataType: 'json',
						data: $('#startjob').serialize()+"&"+$("#delimeter").serialize(),
						beforeSend: function() { 
                                                },
						success: function(data) {
							if(data.success == 'false')
							{  
								$('.horizontalLine').html('Something went to wrong.Please Try again later...');	
							} else {
                                setTimeout(function(){location.href="/"} , 3000);   
                            }
						},
						error: function(xhr, textStatus, thrownError) {
							$('.horizontalLine').html(thrownError);
							
						}
				});
			});

            // $(document).ajaxStop(function () {
            //       window.location.href = './';
            //   });
			
			// setTimeout(function(){location.href="/"} , 1000);   
			//window.location.href = './';
           return false;
		   
		});
		</script>
    @stop
 