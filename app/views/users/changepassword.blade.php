<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>SEO Sheet Processor</title>

    <!-- Bootstrap core CSS -->
    {{ HTML::style('css/main.css') }}

    <!--external css-->
    <!-- {{ HTML::style('css/bootstrap.css') }} -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    {{ HTML::style('font-awesome/css/font-awesome.css') }}

    <!-- blueimp Gallery styles -->
    <link rel="stylesheet" href="css/blueimp-gallery.min.css">

    <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
    <link rel="stylesheet" href="css/jquery.fileupload.css">
    <link rel="stylesheet" href="css/jquery.fileupload-ui.css">

    <!-- {{ HTML::style('js/bootstrap-datepicker/css/datepicker.css') }} -->
    <!-- {{ HTML::style('assets/js/bootstrap-daterangepicker/daterangepicker.css') }} -->
        
    <!-- Custom styles for this template -->
    {{ HTML::style('css/style.css') }}
    {{ HTML::style('css/style-responsive.css') }}
	  {{ HTML::style('src/css/boxy.css'); }}

    <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
    <link rel="stylesheet" href="css/jquery.fileupload.css">
    <link rel="stylesheet" href="css/jquery.fileupload-ui.css">

    <link rel="stylesheet" href="css/jquery.dataTables.min.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') }}
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js') }}
    <![endif]-->
  </head>

  <body>

  <section id="container" >
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<div class="col-md-12">
{{ Form::open(array("action"=>"UsersController@change_password")) }}
    <div class=" " style="min-width:600px;padding:10px 30px;">
    	<div class="changepwdunderline"><h3 class="nomargin">Change Password</h3></div>
     	<div class="input">
            <div class="legend">Old Password</div>
            <div class="textinput">
            	<input type="password" id="oldpwdinput" name="oldpassword" class="form-control">
            	<div class="errormsg" id="oldpwdmsg">Please fill in your password.</div>
            </div>
            <div class="clear"></div>
        </div> 
        <div class="input">
            <div class="legend">New Password</div>
            <div class="textinput">
            	<input type="password" id="pwdinput" name="password" class="form-control">
            	<div class="errormsg" id="pwdmsg">Please fill in your new password.</div>
            </div>
            <div class="clear"></div>
        </div> 
        <div class="input">
            <div class="legend">Confirm Password </div>
            <div class="textinput">
            	<input type="password" id="cfmpwdinput" name="cfmpassword" class="form-control">
            	<div class="errormsg" id="cfmpwdmsg">Please confirm your password.</div>
            </div>
            <div class="clear"></div>
        </div> 
        
        <div class="clear"></div>
        <div class="right">
        	
            <div class="login"><input type="submit" id="submitbtn" class="btn btn-warning" value="Change Password"></div>
        </div>
        <div class="clear"></div>
    </div>
{{ Form::close() }}
</div>

<script>
	var checkoldpwd = false,
		checkpwd = false,
		checkcfmpwd = false;
	function checkOldPwd()
	{
		if ($("#oldpwdinput").val() == "")
		{
			$("#oldpwdmsg").animate({
				opacity:1
			},300);
		}
		else
		{
			$("#oldpwdmsg").animate({
				opacity:0
			},100);
			checkoldpwd = true;
		}
	}
	function checkPwd()
	{
		if ($("#pwdinput").val() == "")
		{
			$("#pwdmsg").animate({
				opacity:1
			},300);
		}
		else
		{
			$("#pwdmsg").animate({
				opacity:0
			},100);
			checkpwd = true;
		}
	}
	function checkCfmPwd()
	{
		if ($("#cfmpwdinput").val() == "")
		{
			$("#cfmpwdmsg").html("Please confirm in your password.");
			$("#cfmpwdmsg").animate({
				opacity:1
			},300);
		}
		else if ($("#cfmpwdinput").val() != $("#pwdinput").val())
		{
			$("#cfmpwdmsg").html("Your entered password is not the same. Please enter the correct password.");
			$("#cfmpwdmsg").animate({
				opacity:1
			},300);
		}
		else
		{
			$("#cfmpwdmsg").animate({
				opacity:0
			},100);
			checkcfmpwd = true;
		}
	}
	$("#oldpwdinput").focusout(function(){
		checkOldPwd();
	});
	$("#pwdinput").focusout(function(){
		checkPwd();
	});
	$("#cfmpwdinput").focusout(function(){
		checkCfmPwd();
	});
	$("form").submit(function(){
		if (checkoldpwd == true && checkpwd == true && checkcfmpwd == true)
		{
			$("#submitbtn").prop('disabled',true);
			return true;
		}
		else
		{
			
			checkOldPwd();
			checkPwd();
			checkCfmPwd();
			
			$(window).scrollTop($('form').offset().top);
			return false;
		}
	});
</script>
</section>
 <!-- js placed at the end of the document so the pages load faster -->
    <!-- {{ HTML::script('js/vendor/user_profile.js') }} -->

    {{ HTML::script('js/jquery-1.11.1.min.js') }}
    {{ HTML::script('js/bootstrap.min.js') }}
    {{ HTML::script('js/jquery.dcjqaccordion.2.7.js') }}
    {{ HTML::script('js/jquery.scrollTo.min.js') }}
    {{ HTML::script('js/jquery.nicescroll.js') }}


    <!--common script for all pages-->
    {{ HTML::script('js/common-scripts.js') }}

    <!--script for this page-->
    {{ HTML::script('js/jquery-ui-1.9.2.custom.min.js') }}

	<!--custom switch-->
	{{ HTML::script('js/bootstrap-switch.js') }}
	
	<!--custom tagsinput-->
	{{ HTML::script('js/jquery.tagsinput.js') }}
	
	<!--custom checkbox & radio-->
	
	<!-- {{ HTML::script('js/bootstrap-datepicker/js/bootstrap-datepicker.js') }} -->
	<!-- {{ HTML::script('js/bootstrap-daterangepicker/date.js') }} -->
	<!-- {{ HTML::script('js/bootstrap-daterangepicker/daterangepicker.js') }} -->
	
	{{ HTML::script('js/bootstrap-inputmask/bootstrap-inputmask.min.js') }}
	
	
	{{ HTML::script('js/form-component.js') }}   
    
  {{ HTML::script("src/js/jquery.boxy.js") }}
  <script src="js/jquery.dataTables.min.js"></script>
 
</body>
</html>