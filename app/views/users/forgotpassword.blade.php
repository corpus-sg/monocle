@extends('layouts.main')

@section('header')

<script src='https://www.google.com/recaptcha/api.js'></script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@stop
@section('content')
<?php
?>
{{ Form::open(array("action"=>"UsersController@forgot_password")) }}
<div class="col-md-12">
    <div class="showback loginregister">
    	<div class="forgotpasswordunderline"><h3 class="nomargin">Forgot Password</h3></div>
       	<div class="padbot">Please type your email to initiate password recovery 
        	<div class="right"><a href="{{ URL::to('register') }}"> Back to login</a></div>
       	</div>
        <div class="input">
            <div class="legend">Email Address </div>
            <div class="textinput "><input type="email" name="email" id="emailinput" class="form-control">
            	
                @if (!isset ($wronglogin))
                	<div class="errormsg" id="emailmsg" >
    	            	Please fill in your email address.
                    </div>
                @else
                	<div class="errormsg" id="emailmsg" style="opacity:1">
	                	Email does not exists.
                    </div>
                @endif
                </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
        
        <div class="input right" style="margin-bottom:20px;">
        	<div class="right">
        	<div class="g-recaptcha" data-sitekey="6Lfv1QcTAAAAAMsvx3SwB297WgSGmmZ7F5qw55U3"></div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <div class="right">
            <div class="login"><input id="submitbtn" type="submit" class="btn btn-warning" value="Submit"></div>
        </div>
        <div class="clear"></div>
    </div>
</div>
{{ Form::close() }}


@stop

@section ('script')
<script>
	var checkemail = false;
	
	function checkEmail()
	{
		if ($("#emailinput").val() == "")
		{
			$("#emailmsg").html("Please fill in your email address.");
			$("#emailmsg").animate({
				opacity:1
			},300);
		}
		else if(!isValidEmailAddress($("#emailinput").val())) 
		{
			$("#emailmsg").html("Your email address is invalid. Please fill in the correct email address.");
			$("#emailmsg").animate({
				opacity:1
			},300);
		}
		else
		{
			
			$("#emailmsg").animate({
				opacity:0
			},100);
			
			checkemail = true;
			$.ajax({
					type: 'post',
					url: './checkemail',
					cache: false,
					dataType: 'json',
					data: 'email='+$("#emailinput").val(),
					beforeSend: function() { 
					},
					success: function(data) {
						if(data.success == false)
						{  
						} 
						else
						{
							if (data == 0)
							{
								$("#emailmsg").html("The email address is not registered.");
								$("#emailmsg").animate({
									opacity:1
								},100);
								checkemail = false;
							}
							else
							{
								
							}
						}
					},
					error: function(xhr, textStatus, thrownError) {
					}
			});
			
		
		}
	}
	
	$("#emailinput").focusout(function(){
		checkEmail();
	});
	$("form").submit(function(){
		if(grecaptcha.getResponse() == "")
		{
			
			$("#emailmsg").html("Please verify using the reCaptcha below.");
			$("#emailmsg").animate({
				opacity:1
			},300);
			return false;
		}
		else
		{
			
			$("#submitbtn").prop('disabled',true);
			return true;
		}
	});
	function isValidEmailAddress(emailAddress) {
		var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
		return pattern.test(emailAddress);
	};
</script>
@stop