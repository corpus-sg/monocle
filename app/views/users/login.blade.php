@extends('layouts.main')

@section('header')

 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@stop
@section('content')
<?php
$wronglogin = Session::get('wronglogin');
$retrievepass = Session::get('message');
?>
{{ Form::open(array("action"=>"UsersController@login")) }}
<div class="col-md-12">
    <div class="showback loginregister">
    	<div class="loginunderline"><h3 class="nomargin">Log In</h3></div>
       	<div class="padbot">Don't have an account? <a href="{{ URL::to('register') }}"> Register here</a><br>
        <div style="color:#0C3;" >
    	           	{{ $retrievepass }}
        </div></div>
        <div class="input">
            <div class="legend">Email Address </div>
            <div class="textinput "><input type="email" name="email" id="emailinput" class="form-control">
            	
                @if (!isset ($wronglogin))
                	<div class="errormsg" id="emailmsg" >
    	            	Please fill in your email address.
                    </div>
                @else
                	<div class="errormsg" id="emailmsg" style="opacity:1">
	                	Wrong username or password.
                    </div>
                @endif
                </div>
            <div class="clear"></div>
        </div>
        <div class="input">
            <div class="legend">Password</div>
            <div class="textinput"><input type="password" name="password" id="pwdinput" class="form-control">
            	<div class="errormsg" id="pwdmsg">Please fill in your password.</div></div>
            <div class="clear"></div>
        </div> 
        <div class="right padbot"><a href="{{ URL::to('forgot_password') }}">Forgot Password</a></div>
        <div class="clear"></div>
        <div class="right">
        	<div class="rememberme"><input type="checkbox" value="yes" name="rememberme" id="rememberme"> <label for="rememberme">Remember Me</label></div>
            <div class="login"><input id="submitbtn" type="submit" class="btn btn-warning" value="Login"></div>
        </div>
        <div class="clear"></div>
    </div>
</div>
{{ Form::close() }}


@stop

@section ('script')
<script>
	var checkemail = false,
		checkpwd = false;
	
	function checkEmail()
	{
		if ($("#emailinput").val() == "")
		{
			$("#emailmsg").html("Please fill in your email address.");
			$("#emailmsg").animate({
				opacity:1
			},300);
		}
		else if(!isValidEmailAddress($("#emailinput").val())) 
		{
			$("#emailmsg").html("Your email address is invalid. Please fill in the correct email address.");
			$("#emailmsg").animate({
				opacity:1
			},300);
		}
		else
		{
			$("#emailmsg").animate({
				opacity:0
			},100);
			checkemail = true;
		}
	}
	function checkPwd()
	{
		if ($("#pwdinput").val() == "")
		{
			$("#pwdmsg").animate({
				opacity:1
			},300);
		}
		else
		{
			$("#pwdmsg").animate({
				opacity:0
			},100);
			checkpwd = true;
		}
	}
	$("#emailinput").focusout(function(){
		checkEmail();
	});
	$("#pwdinput").focusout(function(){
		checkPwd();
	});
	$("form").submit(function(){
		checkEmail();
		checkPwd();
		if (checkemail == true && checkpwd == true)
		{
			$("#submitbtn").prop('disabled',true);
			return true;
		}
		else
		{
			checkEmail();
			checkPwd();
			return false;
		}
	});
	function isValidEmailAddress(emailAddress) {
		var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
		return pattern.test(emailAddress);
	};
</script>
@stop