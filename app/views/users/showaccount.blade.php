@extends('layouts.main')

@section('header')
{{HTML::style('fancybox/jquery.fancybox-1.3.4.css') }}

 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@stop
@section('content')
{{ Form::open(array("action"=>"UsersController@edit_account")) }}
<div class="col-md-12" style="padding-left:0;margin-left:0;">
	<div class="accountsidebar">
    	<div class="sidebaroption">Account Details</div>
    </div>
    <div class="editaccount">
    	<div class="accountdetailsunderline"><h3 class="nomargin">Account Details</h3></div>
        
       	<div class="padbot margtonomarg" >
        	<div class="righttoleft"><input type="button" class="btn btn-warning" value="Edit Details" id="editdetails"> <a class="fancybox" id="changepassword" href="{{URL::to('changepassword')}}"><input type="button" class="btn btn-primary" value="Change Password"></a></div>
            <div class="clear"></div>
       	</div>
        @if (Session::has('message'))
            <div class="padbot" style="color:#0C3;">
                    {{ Session::get('message') }}
                
            </div>
        @endif
        <div class="input">
            <div class="legend">Email Address</div>
            <div class="textinput ">
            	<input type="email" id="emailinput" value="{{ Auth::user()->email }}" name="email" disabled class="transparentinput">
            	<div class="errormsg" id="emailmsg">Please fill in your email address.</div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="input">
            <div class="legend">First Name</div>
            <div class="textinput">
            	<input type="text" id="firstnameinput" value="{{ Auth::user()->first_name }}" name="firstname" disabled class="transparentinput">
            	<div class="errormsg" id="firstnamemsg">Please fill in your first name.</div>
            </div>
            <div class="clear"></div>
        </div> 
        <div class="input">
            <div class="legend">Last Name</div>
            <div class="textinput">
            	<input type="text" id="lastnameinput" value="{{ Auth::user()->last_name }}" name="lastname" disabled class="transparentinput">
            	<div class="errormsg" id="lastnamemsg">Please fill in your last name.</div>
            </div>
            <div class="clear"></div>
        </div> 
        <div class="input">
            <div class="legend">Company</div>
            <div class="textinput">
            	<input type="text" id="companyinput" value="{{ Auth::user()->company }}" name="company" disabled class="transparentinput">
            	<div class="errormsg" id="companymsg">Please fill in your company.</div>
            </div>
            <div class="clear"></div>
        </div> 
        <div class="input">
            <div class="legend">Job Function</div>
            <div class="textinput">
            	<select disabled name="jobfunc" class="transparentinput">
				<?php
					$countries = JobFunctionEntry::orderBy('name')->get();
					foreach ($countries as $key=>$value)
					{
						?>
                        <option @if (Auth::user()->job_function == $value->name) selected @endif value="{{ $value->name }}">{{ $value->name }}</option>
                        <?php
					}
				?>
                </select>
            	<div class="errormsg" id="jobfuncmsg">Please fill in your job function.</div>
             </div>
            <div class="clear"></div>
        </div> 
        <div class="input padbot">
            <div class="legend">Country</div>
            <div class="textinput">
            	<select disabled name="country" class="transparentinput">
				<?php
					$countries = CountryEntry::orderBy('name')->get();
					foreach ($countries as $key=>$value)
					{
						?>
                        <option @if (Auth::user()->country == $value->name) selected @endif value="{{ $value->name }}">{{ $value->name }}</option>
                        <?php
					}
				?>
                </select>
            </div>
            <div class="clear"></div>
        </div> 
        <div class="clear"></div>
        <div class="right">        	
            <div class="login"><input type="submit" style="display:none;" id="submitbtn" class="btn btn-warning" value="Update"></div>
        </div>
        <div class="clear"></div>
    </div>
</div>
{{ Form::close() }}


@stop

@section ('script')
{{ HTML::script('/fancybox/jquery.fancybox-1.3.4.js') }}
<script>

$(document).ready( function () {
	$("#changepassword").fancybox();
});
	$("#editdetails").click(function(){
		$(".transparentinput").prop('disabled',false);
		$(".transparentinput").addClass('form-control');
		$(".transparentinput").removeClass('transparentinput');
		$("#editdetails").prop('disabled',true);
		$("#submitbtn").fadeIn('fast');
	});
	var checkemail = false
		checkfirstname = false,
		checklastname = false,
		checkcompany = false,
		checkjobfunc = false;
	
	function checkEmail()
	{
		if ($("#emailinput").val() == "")
		{
			$("#emailmsg").html("Please fill in your email address.");
			$("#emailmsg").animate({
				opacity:1
			},300);
		}
		else if(!isValidEmailAddress($("#emailinput").val())) 
		{
			$("#emailmsg").html("Your email address is invalid. Please fill in the correct email address.");
			$("#emailmsg").animate({
				opacity:1
			},300);
		}
		else
		{
			$("#emailmsg").animate({
				opacity:0
			},100);
			
			checkemail = true;
			$.ajax({
					type: 'post',
					url: './checkemail',
					cache: false,
					dataType: 'json',
					data: 'email='+$("#emailinput").val(),
					beforeSend: function() { 
					},
					success: function(data) {
						if(data.success == false)
						{  
						} 
						else
						{
							if (data > 0)
							{
								$("#emailmsg").html("This email address is already registered. Please choose another email address.");
								$("#emailmsg").animate({
									opacity:1
								},100);
								checkemail = false;
							}
						}
					},
					error: function(xhr, textStatus, thrownError) {
					}
			});
		}
	}
	function checkFirstName()
	{
		if ($("#firstnameinput").val() == "")
		{
			$("#firstnamemsg").animate({
				opacity:1
			},300);
		}
		else
		{
			$("#firstnamemsg").animate({
				opacity:0
			},100);
			checkfirstname = true;
		}
	}
	function checkLastName()
	{
		if ($("#lastnameinput").val() == "")
		{
			$("#lastnamemsg").animate({
				opacity:1
			},300);
		}
		else
		{
			$("#lastnamemsg").animate({
				opacity:0
			},100);
			checklastname = true;
		}
	}
	function checkCompany()
	{
		if ($("#companyinput").val() == "")
		{
			$("#companymsg").animate({
				opacity:1
			},300);
		}
		else
		{
			$("#companymsg").animate({
				opacity:0
			},100);
			checkcompany = true;
		}
	}
	function checkJobFunc()
	{
		if ($("#jobfuncinput").val() == "")
		{
			$("#jobfuncmsg").animate({
				opacity:1
			},300);
		}
		else
		{
			$("#jobfuncmsg").animate({
				opacity:0
			},100);
			checkjobfunc = true;
		}
	}
	$("#emailinput").focusout(function(){
		checkEmail();
	});
	$("#firstnameinput").focusout(function(){
		checkFirstName();
	});
	$("#lastnameinput").focusout(function(){
		checkLastName();
	});
	$("#companyinput").focusout(function(){
		checkCompany();
	});
	function isValidEmailAddress(emailAddress) {
		var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
		return pattern.test(emailAddress);
	};
	$("form").submit(function(){
		
			checkEmail();
			checkFirstName();
			checkLastName();
			checkCompany();
		if (checkemail == true && checkfirstname == true && checklastname == true && checkcompany == true)
		{
			$("#submitbtn").prop('disabled',true);
			return true;
		}
		else
		{
			
			
			$(window).scrollTop($('form').offset().top);
			return false;
		}
	});
</script>
@stop