$(window).resize(function(){
    if ($(window).width() >= 767)
    {
        $("#menu-content").removeClass("out");
        $("#menu-content").addClass("in");
    }
});
$("#sall").click(function(){
    $('.delimiter').prop('checked', true);
});
$("#usall").click(function(){
    $('.delimiter').prop('checked', false);
});
$("#normal").click(function(){
    $("#title").html('<i class="fa fa- fa-file"></i> Normal Sheet</h3><div id="test-1.csv">');
    $("#type").val(1);
});
$("#google").click(function(){
    $("#title").html('<i class="fa fa- fa-google"></i> Google Webmaster</h3><div id="test-1.csv">');
    $("#type").val(2);
});
$("#web").click(function(){
    $("#title").html('<i class="fa fa- fa-globe"></i> Similiar Web</h3><div id="test-1.csv">');
    $("#type").val(3);
});
$(document).ready(function() {
  $.ajax({
            type: 'post',
            url: 'reset.php'
    });
});
$( window ).unload(function() {
  $.ajax({
            type: 'post',
            url: 'reset.php'
    });
});
/*
 * jQuery File Upload Plugin JS Example 8.9.1
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global $, window */
var ids = 0;
var cntupload = 0,cntdone = 0;
$(function ($) {
    'use strict';

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: 'upload',
        autoUpload: false,
        acceptFileTypes: /(\.|\/)(xlsx|xls|csv)$/i,
        maxFileSize: 5000000,
    });
    var fileName = [],fileSize =[];
	var checker = false;

    // Enable iframe cross-domain access via redirect option:
    $('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/html/result.html?%s'
        )
        ).bind('fileuploaddone', function(e, data) {
			
		setTimeout(function(){ 
					if (!checker)
					{
						$('table tr td p').each(function(){
							if($(this).find('a').eq(0).text() != ""){
								 $("button").prop("disabled",false);
								 checker = true;
							}
						});
					}
					cntdone++;
					if (cntdone > 1)
					{
						$("#deleteallfiles").fadeIn();
					}
				$(".deletebtn").click(function(){
					var name = $(this).attr('data-url').split('/');
					$('table tr td p').each(function(){
						if($(this).find('span').eq(0).text() == name[2]){
							 $(this).closest('tr').remove();
						}
						if($(this).find('a').eq(0).text() == name[2]){
							 $(this).closest('tr').remove();
						}
					});
						var found = 1;
					$('table tr td p').each(function(){
						if($(this).find('a').eq(0).text()){
							 found++;
						}
					});
					if (found == 1)
					{
						$(".starter").prop("disabled",true);
					}
					else
					{
						$(".starter").prop("disabled",false);
					}
                                       $.ajax({
							type: 'post',
							data: 'files='+$(this).attr('data-url'),
							url: 'unlink.php'
					});
				});

				
                                       
			 }, 300);	
            // TODO

        }).bind('fileuploadfail', function(e, data) {
            // Show error message
    }).bind('fileuploadadd', function (e, data) {
		cntupload++;
		if (cntupload > 1)
		{
			$("#uploadallfiles").fadeIn('slow');
			$("#cancelallfiles").fadeIn('slow');
		}
		
	});

    if (window.location.hostname === 'blueimp.github.io') {
        // Demo settings:
        $('#fileupload').fileupload('option', {
            url: '//jquery-file-upload.appspot.com/',
            // Enable image resizing, except for Android and Opera,
            // which actually support image resizing, but fail to
            // send Blob objects via XHR requests:
            disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator.userAgent),
        acceptFileTypes: /(\.|\/)(xlsx|xls|csv)$/i,
        maxFileSize: 5000000
        });
        // Upload server status check for browsers with CORS support:
        if ($.support.cors) {
            $.ajax({
                url: '//jquery-file-upload.appspot.com/',
                type: 'HEAD'
            }).fail(function () {
                $('<div class="alert alert-danger"/>')
                    .text('Upload server currently unavailable - ' +
                            new Date())
                    .prependTo('#fileupload');
            });
        }
    } else {
        // Load existing files:
        $('#fileupload').addClass('fileupload-processing');
        $.ajax({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            url: $('#fileupload').fileupload('option', 'url'),
            dataType: 'json',
            context: $('#fileupload')[0]
        }).always(function () {
            $(this).removeClass('fileupload-processing');
        }).done(function (result) {
            $(this).fileupload('option', 'done')
                .call(this, $.Event('done'), {result: result});
        });
    }

});